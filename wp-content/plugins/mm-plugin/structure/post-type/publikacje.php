<?php

function mm_posttype_publications() {
	$labels = array(
		'name'               => _x( 'Publikacje', '', 'mm-plugin' ),
		'singular_name'      => _x( 'Publikacje', '', 'mm-plugin' ),
		'menu_name'          => _x( 'Publikacje', '', 'mm-plugin' ),
		'name_admin_bar'     => _x( 'Publikacje', '', 'mm-plugin' ),
		'add_new'            => _x( 'Dodaj publikacje', '', 'mm-plugin' ),
		'add_new_item'       => __( 'Dodaj nowe Publikacje', 'mm-plugin' ),
		'new_item'           => __( 'Nowa publikacja', 'mm-plugin' ),
		'edit_item'          => __( 'Edytuj Publikacje', 'mm-plugin' ),
		'view_item'          => __( 'Zobacz publikacje', 'mm-plugin' ),
		'all_items'          => __( 'Wszystkie publikacje', 'mm-plugin' ),
		'search_items'       => __( 'Szukaj publikacji', 'mm-plugin' ),
		'parent_item_colon'  => __( 'Rodzic publikacji:', 'mm-plugin' ),
		'not_found'          => __( 'Nie znaleziono żadnych publikacji', 'mm-plugin' ),
		'not_found_in_trash' => __( 'Nie znaleziono żadnych publikacji', 'mm-plugin' )
	);
	$rewrite = array(
		'slug'                => 'publikacje2',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'labels'             => $labels,
        'description'        => __( 'publikacje', 'mm-plugin' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'show_in_nav_menus'  => true,
		'query_var'          => true,
		'rewrite'            => $rewrite,
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_position'       => 7,
		'menu_icon'  		 => 'dashicons-admin-links',
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt','revisions')
	);
	register_post_type( 'publications', apply_filters('mm-theme/plugin/post-type/publications', $args) );
}
add_action( 'init', 'mm_posttype_publications', 0 );

function taxonomies_publications_cat() {
$labels = array(
    'name'              => _x( 'Dodaj kategorię', 'taxonomy general name' ),
    'singular_name'     => _x( 'Kategoria', 'taxonomy singular name' ),
    'search_items'      => __( 'Szukaj' ),
    'all_items'         => __( 'Wszyscy' ),
    'parent_item'       => __( '' ),
    'parent_item_colon' => __( 'Rodzic kategorii' ),
    'edit_item'         => __( 'Edytuj' ),
    'update_item'       => __( 'Edytuj kategorię' ),
    'add_new_item'      => __( 'Dodaj kategorię' ),
    'new_item_name'     => __( 'Nowa' ),
    'menu_name'         => __( 'Kategorie' ),
    );

$args = array(
    'labels' => $labels,
    'hierarchical' => true,
    );

register_taxonomy( 'taxonomies_publications_cat', 'publications', $args );
}
add_action( 'init', 'taxonomies_publications_cat', 0 );
