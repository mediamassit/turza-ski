<?php

function mm_posttype_protocols() {
	$labels = array(
		'name'               => _x( 'Z pracy rady miasta', '', 'mm-plugin' ),
		'singular_name'      => _x( 'Z pracy rady miasta', '', 'mm-plugin' ),
		'menu_name'          => _x( 'Z pracy Rady Miasta', '', 'mm-plugin' ),
		'name_admin_bar'     => _x( 'Z pracy', '', 'mm-plugin' ),
		'add_new'            => _x( 'Dodaj', '', 'mm-plugin' ),
		'add_new_item'       => __( 'Dodaj nowe', 'mm-plugin' ),
		'new_item'           => __( 'Nowy', 'mm-plugin' ),
		'edit_item'          => __( 'Edytuj', 'mm-plugin' ),
		'view_item'          => __( 'Zobacz', 'mm-plugin' ),
		'all_items'          => __( 'Wszystkie', 'mm-plugin' ),
		'search_items'       => __( 'Szukaj', 'mm-plugin' ),
		'parent_item_colon'  => __( 'Rodzic:', 'mm-plugin' ),
		'not_found'          => __( 'Nie znaleziono wniosków.', 'mm-plugin' ),
		'not_found_in_trash' => __( 'Nie znaleziono wniosków.', 'mm-plugin' )
	);
	$rewrite = array(
		'slug'                => 'protocols',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Z pracy Rady Miasta', 'mm-plugin' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => $rewrite,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_position'       => 7,
		'menu_icon'  		 => 'dashicons-search',
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt')
	);
	register_post_type( 'protocols', apply_filters('mm-theme/plugin/post-type/protocols', $args) );
}
add_action( 'init', 'mm_posttype_protocols', 0 );


function taxonomies_years_fromwork() {
$labels = array(
    'name'              => _x( 'Lata', 'taxonomy general name' ),
    'singular_name'     => _x( 'Lata', 'taxonomy singular name' ),
    'search_items'      => __( 'Szukaj' ),
    'all_items'         => __( 'Wszystkie' ),
    'parent_item'       => __( '' ),
    'parent_item_colon' => __( 'Rodzic ' ),
    'edit_item'         => __( 'Edytuj' ),
    'update_item'       => __( 'Edytuj Lata' ),
    'add_new_item'      => __( 'Dodaj Lata' ),
    'new_item_name'     => __( 'Nowy' ),
    'menu_name'         => __( 'Lata' ),
    );

$args = array(
    'labels' => $labels,
    'hierarchical' => true,
    );

register_taxonomy( 'years_category_protocols', 'protocols', $args );
}
add_action( 'init', 'taxonomies_years_fromwork', 0 );
