<?php

function mm_posttype_elections() {
	$labels = array(
		'name'               => _x( 'Wybory', '', 'mm-plugin' ),
		'singular_name'      => _x( 'Wybory', '', 'mm-plugin' ),
		'menu_name'          => _x( 'Wybory ', '', 'mm-plugin' ),
		'name_admin_bar'     => _x( 'Wybory', '', 'mm-plugin' ),
		'add_new'            => _x( 'Dodaj Aktualność', '', 'mm-plugin' ),
		'add_new_item'       => __( 'Dodaj nową Aktualność', 'mm-plugin' ),
		'new_item'           => __( 'Nowa Aktualność', 'mm-plugin' ),
		'edit_item'          => __( 'Edytuj Aktualność', 'mm-plugin' ),
		'view_item'          => __( 'Zobacz Aktualność', 'mm-plugin' ),
		'all_items'          => __( 'Wszystkie Aktualności', 'mm-plugin' ),
		'search_items'       => __( 'Szukaj Aktualności', 'mm-plugin' ),
		'parent_item_colon'  => __( 'Rodzic Aktualności:', 'mm-plugin' ),
		'not_found'          => __( 'Nie znaleziono Aktualności.', 'mm-plugin' ),
		'not_found_in_trash' => __( 'Nie znaleziono Aktualności.', 'mm-plugin' )
	);
	$rewrite = array(
		'slug'                => 'elections',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Wybory', 'mm-plugin' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => $rewrite,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_position'       => 7,
		'menu_icon'  		 => 'dashicons-forms',
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt','comments','revisions')
	);
	register_post_type( 'elections', apply_filters('mm-theme/plugin/post-type/elections', $args) );
}
add_action( 'init', 'mm_posttype_elections', 0 );
