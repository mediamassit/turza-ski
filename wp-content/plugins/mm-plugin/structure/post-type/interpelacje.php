<?php

function mm_posttype_interpelations() {
	$labels = array(
		'name'               => _x( 'Interpelacje i wnioski', '', 'mm-plugin' ),
		'singular_name'      => _x( 'Wnioski', '', 'mm-plugin' ),
		'menu_name'          => _x( 'Interpelacje i wnioski', '', 'mm-plugin' ),
		'name_admin_bar'     => _x( 'Wnioski', '', 'mm-plugin' ),
		'add_new'            => _x( 'Dodaj wniosek', '', 'mm-plugin' ),
		'add_new_item'       => __( 'Dodaj nowy wniosek', 'mm-plugin' ),
		'new_item'           => __( 'Nowy wniosek', 'mm-plugin' ),
		'edit_item'          => __( 'Edytuj wniosek', 'mm-plugin' ),
		'view_item'          => __( 'Zobacz wniosek', 'mm-plugin' ),
		'all_items'          => __( 'Wszystkie wnioski', 'mm-plugin' ),
		'search_items'       => __( 'Szukaj wniosków', 'mm-plugin' ),
		'parent_item_colon'  => __( 'Rodzic wniosku:', 'mm-plugin' ),
		'not_found'          => __( 'Nie znaleziono wniosków.', 'mm-plugin' ),
		'not_found_in_trash' => __( 'Nie znaleziono wniosków.', 'mm-plugin' )
	);
	$rewrite = array(
		'slug'                => 'wnioski',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Interpelacje i wnioski', 'mm-plugin' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => $rewrite,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_position'       => 7,
		'menu_icon'  		 => 'dashicons-format-quote',
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt','revisions')
	);
	register_post_type( 'interpelations', apply_filters('mm-theme/plugin/post-type/test', $args) );
}
add_action( 'init', 'mm_posttype_interpelations', 0 );

function taxonomies_radni() {
$labels = array(
    'name'              => _x( 'Radni', 'taxonomy general name' ),
    'singular_name'     => _x( 'Radny', 'taxonomy singular name' ),
    'search_items'      => __( 'Szukaj' ),
    'all_items'         => __( 'Wszyscy' ),
    'parent_item'       => __( '' ),
    'parent_item_colon' => __( 'Rodzic Radnego' ),
    'edit_item'         => __( 'Edytuj' ),
    'update_item'       => __( 'Edytuj Radnego' ),
    'add_new_item'      => __( 'Dodaj Radnego' ),
    'new_item_name'     => __( 'Nowy' ),
    'menu_name'         => __( 'Radni' ),
    );

$args = array(
    'labels' => $labels,
    'hierarchical' => true,
    );

register_taxonomy( 'radni_category', 'interpelations', $args );
}
add_action( 'init', 'taxonomies_radni', 0 );

function taxonomies_years() {
$labels = array(
    'name'              => _x( 'Lata', 'taxonomy general name' ),
    'singular_name'     => _x( 'Lata', 'taxonomy singular name' ),
    'search_items'      => __( 'Szukaj' ),
    'all_items'         => __( 'Wszystkie' ),
    'parent_item'       => __( '' ),
    'parent_item_colon' => __( 'Rodzic ' ),
    'edit_item'         => __( 'Edytuj' ),
    'update_item'       => __( 'Edytuj Lata' ),
    'add_new_item'      => __( 'Dodaj Lata' ),
    'new_item_name'     => __( 'Nowy' ),
    'menu_name'         => __( 'Lata' ),
    );

$args = array(
    'labels' => $labels,
    'hierarchical' => true,
    );

register_taxonomy( 'years_category', 'interpelations', $args );
}
add_action( 'init', 'taxonomies_years', 0 );
