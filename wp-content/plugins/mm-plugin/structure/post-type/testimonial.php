<?php

function mm_posttype_testimonial() {
	$labels = array(
		'name'               => _x( 'Opinie', '', 'mm-plugin' ),
		'singular_name'      => _x( 'Opinia', '', 'mm-plugin' ),
		'menu_name'          => _x( 'Opinie', '', 'mm-plugin' ),
		'name_admin_bar'     => _x( 'Opinie', '', 'mm-plugin' ),
		'add_new'            => _x( 'Dodaj opinię', '', 'mm-plugin' ),
		'add_new_item'       => __( 'Dodaj nową opinię', 'mm-plugin' ),
		'new_item'           => __( 'Nowa opinia', 'mm-plugin' ),
		'edit_item'          => __( 'Edytuj opinię', 'mm-plugin' ),
		'view_item'          => __( 'Zobacz opinię', 'mm-plugin' ),
		'all_items'          => __( 'Wszystkie opinie', 'mm-plugin' ),
		'search_items'       => __( 'Szukaj opinii', 'mm-plugin' ),
		'parent_item_colon'  => __( 'Rodzic opinii:', 'mm-plugin' ),
		'not_found'          => __( 'Nie znaleziono opinii.', 'mm-plugin' ),
		'not_found_in_trash' => __( 'Nie znaleziono opinii w koszu.', 'mm-plugin' )
	);
	$rewrite = array(
		'slug'                => 'opinia',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Opis.', 'mm-plugin' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => $rewrite,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_position'       => 7,
		'menu_icon'  		 => 'dashicons-list-view',
		'supports'           => array( 'title', 'editor', 'thumbnail')
	);
	register_post_type( 'testimonial', apply_filters('mm-theme/plugin/post-type/testimonial', $args) );
}
add_action( 'init', 'mm_posttype_testimonial', 0 );