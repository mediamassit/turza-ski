<?php

function mm_posttype_councilors() {
	$labels = array(
		'name'               => _x( 'Radni', '', 'mm-plugin' ),
		'singular_name'      => _x( 'Radni', '', 'mm-plugin' ),
		'menu_name'          => _x( 'Radni', '', 'mm-plugin' ),
		'name_admin_bar'     => _x( 'Usltestugi', '', 'mm-plugin' ),
		'add_new'            => _x( 'Dodaj Radnego', '', 'mm-plugin' ),
		'add_new_item'       => __( 'Dodaj nowego Radnego', 'mm-plugin' ),
		'new_item'           => __( 'Nowy Radny', 'mm-plugin' ),
		'edit_item'          => __( 'Edytuj Radnego', 'mm-plugin' ),
		'view_item'          => __( 'Zobacz Radnego', 'mm-plugin' ),
		'all_items'          => __( 'Wszyscy Radni', 'mm-plugin' ),
		'search_items'       => __( 'Szukaj Radnego', 'mm-plugin' ),
		'parent_item_colon'  => __( 'Rodzic :', 'mm-plugin' ),
		'not_found'          => __( 'Nie znaleziono Radnych.', 'mm-plugin' ),
		'not_found_in_trash' => __( 'Nie znaleziono nic w koszu.', 'mm-plugin' )
	);
	$rewrite = array(
		'slug'                => 'radni',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'labels'             => $labels,
        'description'        => __( 'radni.', 'mm-plugin' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => $rewrite,
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_position'       => 7,
		'menu_icon'  		 => 'dashicons-groups',
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt')
	);
	register_post_type( 'councilors', apply_filters('mm-theme/plugin/post-type/list_councilors', $args) );
}
add_action( 'init', 'mm_posttype_councilors', 0 );
