<?php

function mm_posttype_materialy() {
	$labels = array(
		'name'               => _x( 'Materiały filmowe', '', 'mm-plugin' ),
		'singular_name'      => _x( 'Film', '', 'mm-plugin' ),
		'menu_name'          => _x( 'Materiały filmowe', '', 'mm-plugin' ),
		'name_admin_bar'     => _x( 'Filmy', '', 'mm-plugin' ),
		'add_new'            => _x( 'Dodaj materiał', '', 'mm-plugin' ),
		'add_new_item'       => __( 'Dodaj nowy materiał', 'mm-plugin' ),
		'new_item'           => __( 'Nowy materiał', 'mm-plugin' ),
		'edit_item'          => __( 'Edytuj materiał', 'mm-plugin' ),
		'view_item'          => __( 'Zobacz materiał', 'mm-plugin' ),
		'all_items'          => __( 'Wszystkie materiały', 'mm-plugin' ),
		'search_items'       => __( 'Szukaj materiałów', 'mm-plugin' ),
		'parent_item_colon'  => __( 'Rodzic materiału:', 'mm-plugin' ),
		'not_found'          => __( 'Nie znaleziono materiałów.', 'mm-plugin' ),
		'not_found_in_trash' => __( 'Nie znaleziono materiałów.', 'mm-plugin' )
	);
	$rewrite = array(
		'slug'                => 'materialy',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Materiały filmowe', 'mm-plugin' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => $rewrite,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_position'       => 7,
		'menu_icon'  		 => 'dashicons-video-alt3',
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt')
	);
	register_post_type( 'videos', apply_filters('mm-theme/plugin/post-type/materialy', $args) );
}
add_action( 'init', 'mm_posttype_materialy', 0 );

function taxonomies_video() {
$labels = array(
    'name'              => _x( 'Kategorie', 'taxonomy general name' ),
    'singular_name'     => _x( 'Kategoria', 'taxonomy singular name' ),
    'search_items'      => __( 'Szukaj' ),
    'all_items'         => __( 'Wszyscy' ),
    'parent_item'       => __( '' ),
    'parent_item_colon' => __( 'Rodzic Kategorii' ),
    'edit_item'         => __( 'Edytuj' ),
    'update_item'       => __( 'Edytuj Kategorię' ),
    'add_new_item'      => __( 'Dodaj Kategorię' ),
    'new_item_name'     => __( 'Nowa' ),
    'menu_name'         => __( 'Kategorie' ),
    );

$args = array(
    'labels' => $labels,
    'hierarchical' => true,
    );

register_taxonomy( 'kategoria_video', 'videos', $args );
}
add_action( 'init', 'taxonomies_video', 0 );