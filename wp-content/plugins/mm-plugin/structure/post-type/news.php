<?php

function mm_posttype_news() {
	$labels = array(
		'name'               => _x( 'Aktualności', '', 'mm-plugin' ),
		'singular_name'      => _x( 'Aktualności', '', 'mm-plugin' ),
		'menu_name'          => _x( 'Aktualności ', '', 'mm-plugin' ),
		'name_admin_bar'     => _x( 'Aktualności', '', 'mm-plugin' ),
		'add_new'            => _x( 'Dodaj Aktualność', '', 'mm-plugin' ),
		'add_new_item'       => __( 'Dodaj nową Aktualność', 'mm-plugin' ),
		'new_item'           => __( 'Nowa Aktualność', 'mm-plugin' ),
		'edit_item'          => __( 'Edytuj Aktualność', 'mm-plugin' ),
		'view_item'          => __( 'Zobacz Aktualność', 'mm-plugin' ),
		'all_items'          => __( 'Wszystkie Aktualności', 'mm-plugin' ),
		'search_items'       => __( 'Szukaj Aktualności', 'mm-plugin' ),
		'parent_item_colon'  => __( 'Rodzic Aktualności:', 'mm-plugin' ),
		'not_found'          => __( 'Nie znaleziono Aktualności.', 'mm-plugin' ),
		'not_found_in_trash' => __( 'Nie znaleziono Aktualności.', 'mm-plugin' )
	);
	$rewrite = array(
		'slug'                => 'aktualnosci',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Aktualności', 'mm-plugin' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => $rewrite,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_position'       => 7,
		'menu_icon'  		 => 'dashicons-megaphone',
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt','comments','revisions')
	);
	register_post_type( 'news', apply_filters('mm-theme/plugin/post-type/news', $args) );
}
add_action( 'init', 'mm_posttype_news', 0 );
