<?php

function mm_posttype_resolutions() {
	$labels = array(
		'name'               => _x( 'Projekty uchwał', '', 'mm-plugin' ),
		'singular_name'      => _x( 'Projekty uchwał', '', 'mm-plugin' ),
		'menu_name'          => _x( 'Projekty uchwał', '', 'mm-plugin' ),
		'name_admin_bar'     => _x( 'Projekt', '', 'mm-plugin' ),
		'add_new'            => _x( 'Dodaj', '', 'mm-plugin' ),
		'add_new_item'       => __( 'Dodaj nowy projekt', 'mm-plugin' ),
		'new_item'           => __( 'Nowy projekt', 'mm-plugin' ),
		'edit_item'          => __( 'Edytuj projekt', 'mm-plugin' ),
		'view_item'          => __( 'Zobacz projekt', 'mm-plugin' ),
		'all_items'          => __( 'Wszystkie projekty', 'mm-plugin' ),
		'search_items'       => __( 'Szukaj', 'mm-plugin' ),
		'parent_item_colon'  => __( 'Rodzic:', 'mm-plugin' ),
		'not_found'          => __( 'Nie znaleziono projektów.', 'mm-plugin' ),
		'not_found_in_trash' => __( 'Nie znaleziono projektów.', 'mm-plugin' )
	);
	$rewrite = array(
		'slug'                => 'resolutions',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'labels'             => $labels,
        'description'        => __( 'resolutions', 'mm-plugin' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => $rewrite,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_position'       => 7,
		'menu_icon'  		 => 'dashicons-media-default',
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt','revisions')
	);
	register_post_type( 'resolutions', apply_filters('mm-theme/plugin/post-type/resolutions', $args) );
}
add_action( 'init', 'mm_posttype_resolutions', 0 );


function taxonomies_years_resolutions() {
$labels = array(
    'name'              => _x( 'Lata', 'taxonomy general name' ),
    'singular_name'     => _x( 'Lata', 'taxonomy singular name' ),
    'search_items'      => __( 'Szukaj' ),
    'all_items'         => __( 'Wszystkie' ),
    'parent_item'       => __( '' ),
    'parent_item_colon' => __( 'Rodzic ' ),
    'edit_item'         => __( 'Edytuj' ),
    'update_item'       => __( 'Edytuj Lata' ),
    'add_new_item'      => __( 'Dodaj Lata' ),
    'new_item_name'     => __( 'Nowy' ),
    'menu_name'         => __( 'Lata' ),
    );

$args = array(
    'labels' => $labels,
    'hierarchical' => true,
    );

register_taxonomy( 'years_category_resolutions', 'resolutions', $args );
}
add_action( 'init', 'taxonomies_years_resolutions', 0 );
