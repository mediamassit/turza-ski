<?php

function mm_posttype_galeria() {
	$labels = array(
		'name'               => _x( 'Galeria', '', 'mm-plugin' ),
		'singular_name'      => _x( 'Galeria', '', 'mm-plugin' ),
		'menu_name'          => _x( 'Galeria', '', 'mm-plugin' ),
		'name_admin_bar'     => _x( 'Galeria', '', 'mm-plugin' ),
		'add_new'            => _x( 'Dodaj galerie', '', 'mm-plugin' ),
		'add_new_item'       => __( 'Dodaj nową galerie', 'mm-plugin' ),
		'new_item'           => __( 'Nowa galeria', 'mm-plugin' ),
		'edit_item'          => __( 'Edytuj galerie', 'mm-plugin' ),
		'view_item'          => __( 'Zobacz galerie', 'mm-plugin' ),
		'all_items'          => __( 'Wszystkie galerie', 'mm-plugin' ),
		'search_items'       => __( 'Szukaj galerii', 'mm-plugin' ),
		'parent_item_colon'  => __( 'Rodzic galerii:', 'mm-plugin' ),
		'not_found'          => __( 'Nie znaleziono żadnych galerii.', 'mm-plugin' ),
		'not_found_in_trash' => __( 'Nie znaleziono żadnych galerii.', 'mm-plugin' )
	);
	$rewrite = array(
		'slug'                => 'gallery',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'labels'             => $labels,
        'description'        => __( 'gallery', 'mm-plugin' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => $rewrite,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_position'       => 7,
		'menu_icon'  		 => 'dashicons-format-image',
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt','comments','revisions')
	);
	register_post_type( 'gallery', apply_filters('mm-theme/plugin/post-type/galeria', $args) );
}
add_action( 'init', 'mm_posttype_galeria', 0 );
