<?php

function mm_posttype_test() {
	$labels = array(
		'name'               => _x( 'Projekt UMWETU', '', 'mm-plugin' ),
		'singular_name'      => _x( 'Projekt', '', 'mm-plugin' ),
		'menu_name'          => _x( 'Projekt UMWETU', '', 'mm-plugin' ),
		'name_admin_bar'     => _x( 'Posty', '', 'mm-plugin' ),
		'add_new'            => _x( 'Dodaj post', '', 'mm-plugin' ),
		'add_new_item'       => __( 'Dodaj nowy post', 'mm-plugin' ),
		'new_item'           => __( 'Nowy post', 'mm-plugin' ),
		'edit_item'          => __( 'Edytuj post', 'mm-plugin' ),
		'view_item'          => __( 'Zobacz post', 'mm-plugin' ),
		'all_items'          => __( 'Wszystkie posty', 'mm-plugin' ),
		'search_items'       => __( 'Szukaj postów', 'mm-plugin' ),
		'parent_item_colon'  => __( 'Rodzic oferty:', 'mm-plugin' ),
		'not_found'          => __( 'Nie znaleziono postów.', 'mm-plugin' ),
		'not_found_in_trash' => __( 'Nie znaleziono postów.', 'mm-plugin' )
	);
	$rewrite = array(
		'slug'                => 'projekty',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Projekt UMWETU', 'mm-plugin' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => $rewrite,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_position'       => 7,
		'menu_icon'  		 => 'dashicons-book',
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt','comments','revisions')
	);
	register_post_type( 'project', apply_filters('mm-theme/plugin/post-type/test', $args) );
}
add_action( 'init', 'mm_posttype_test', 0 );

function taxonomies_kategorie() {
$labels = array(
    'name'              => _x( 'Dodaj kategorię', 'taxonomy general name' ),
    'singular_name'     => _x( 'Kategoria', 'taxonomy singular name' ),
    'search_items'      => __( 'Szukaj' ),
    'all_items'         => __( 'Wszyscy' ),
    'parent_item'       => __( '' ),
    'parent_item_colon' => __( 'Rodzic kategorii' ),
    'edit_item'         => __( 'Edytuj' ),
    'update_item'       => __( 'Edytuj kategorię' ),
    'add_new_item'      => __( 'Dodaj kategorię' ),
    'new_item_name'     => __( 'Nowa' ),
    'menu_name'         => __( 'Kategorie' ),
    );

$args = array(
    'labels' => $labels,
    'hierarchical' => true,
    );

register_taxonomy( 'kategorie_category', 'project', $args );
}
add_action( 'init', 'taxonomies_kategorie', 0 );
