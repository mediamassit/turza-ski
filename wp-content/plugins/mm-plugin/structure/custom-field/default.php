<?php
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(apply_filters('mm/acf/add-local-field-group/group_56f54963ee1412', array(
	'key' => 'group_56f54963ee1412',
	'title' => 'Domyślny szablon',
	'fields' => array (
		array (
			'key' => 'field_570641274350e',
			'label' => 'Wyłącz formatowanie',
			'name' => 'remove_format',
			'type' => 'select',
			'instructions' => 'Usuwa znaczniki paragrafów.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => 'col-1-2',
				'id' => '',
			),
			'choices' => array (
				'0' => 'Nie',
				'1' => 'Tak',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
			'disabled' => 0,
			'readonly' => 0,
		),
		array (
			'key' => 'field_570272419b835',
			'label' => 'Załączniki',
			'name' => 'attachments',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => '',
			'min' => '',
			'max' => '',
			'layout' => 'table',
			'button_label' => 'Dodaj wiersz',
			'sub_fields' => array (
				array (
					'key' => 'field_570272899b836',
					'label' => 'Plik',
					'name' => 'file',
					'type' => 'file',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => 50,
						'class' => '',
						'id' => '',
					),
					'return_format' => 'array',
					'library' => 'all',
					'min_size' => '',
					'max_size' => '',
					'mime_types' => '',
				),
				array (
					'key' => 'field_570272ea9b837',
					'label' => 'Nazwa',
					'name' => 'name',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => 50,
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'page',
			),
		),
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
		),
	),
	'menu_order' => 3,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
)));

endif;