<?php

if( function_exists('acf_add_local_field_group') ):

  acf_add_local_field_group(apply_filters('mm/acf/add-local-field-group/group_588b1184b6ee9', array(
    'key' => 'group_588b1184b6ee9',
      'title' => 'Materiały filmowe',
      'fields' => array (
		array (
			'key' => 'field_588b119896545',
			'label' => 'Tytuł podstrony',
			'name' => 'title_materials',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_588b11ac96546',
			'label' => 'Opis podstrony',
			'name' => 'desc_materials',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'page_template',
				'operator' => '==',
				'value' => 'page-templates/template-videos.php',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
)));

endif;