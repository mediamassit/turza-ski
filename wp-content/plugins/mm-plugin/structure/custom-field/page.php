<?php

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_al4470aa31416',
	'title' => 'Wybierz sidebar',
	'fields' => array (
		array (
			'key' => 'field_al4470c499ece',
			'label' => 'Wybierz sidebar',
			'name' => 'wybierz_sidebar',
			'type' => 'sidebar_selector',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'allow_null' => 1,
			'default_value' => '',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'page_template',
				'operator' => '==',
				'value' => 'default',
			),
		),

	),
	'menu_order' => 0,
	'position' => 'side',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;