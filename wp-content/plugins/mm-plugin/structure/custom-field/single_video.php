<?php

if( function_exists('acf_add_local_field_group') ):

  acf_add_local_field_group(apply_filters('mm/acf/add-local-field-group/group_tryb1184b6ee9', array(
    'key' => 'group_tryb1184b6ee9',
      'title' => 'Materiał filmowy z YT',
      'fields' => array (
		array (
			'key' => 'field_tryb119896545',
			'label' => 'Link do filmu',
			'name' => 'id_yt',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'videos',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
)));

endif;