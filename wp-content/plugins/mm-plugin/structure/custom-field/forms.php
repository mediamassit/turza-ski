<?php
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(apply_filters('mm/acf/add-local-field-group/group_57026c43e43ae', array(
	'key' => 'group_57026c43e43ae',
	'title' => 'Formularze',
	'fields' => array (
		array (
			'key' => 'field_57026caffa955',
			'label' => 'Formularze',
			'name' => 'forms',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => '',
			'min' => '',
			'max' => '',
			'layout' => 'table',
			'button_label' => 'Dodaj formularz',
			'sub_fields' => array (
				array (
					'key' => 'field_57026cc9fa956',
					'label' => 'Formularz',
					'name' => 'contact_form',
					'type' => 'post_object',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'post_type' => array (
						0 => 'wpcf7_contact_form',
					),
					'taxonomy' => array (
					),
					'allow_null' => 0,
					'multiple' => 0,
					'return_format' => 'id',
					'ui' => 1,
				),
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
		),
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'page',
			),
		),
	),
	'menu_order' => 2,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
)));

endif;