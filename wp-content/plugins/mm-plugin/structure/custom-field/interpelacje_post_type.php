<?php 
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_as8f4051e6d26',
	'title' => 'Dodatkowe opcje',
	'fields' => array (
//		array (
//			'key' => 'field_589043d0b31f5',
//			'label' => 'Tytuł',
//			'name' => 'title_inter',
//			'type' => 'text',
//			'instructions' => '',
//			'required' => 0,
//			'conditional_logic' => 0,
//			'wrapper' => array (
//				'width' => '',
//				'class' => '',
//				'id' => '',
//			),
//			'default_value' => '',
//			'placeholder' => '',
//			'prepend' => '',
//			'append' => '',
//			'maxlength' => '',
//			'readonly' => 0,
//			'disabled' => 0,
//		),
//		array (
//			'key' => 'field_589043e3b31f6',
//			'label' => 'Zawartość',
//			'name' => 'text_inter',
//			'type' => 'wysiwyg',
//			'instructions' => '',
//			'required' => 0,
//			'conditional_logic' => 0,
//			'wrapper' => array (
//				'width' => '',
//				'class' => '',
//				'id' => '',
//			),
//			'default_value' => '',
//			'tabs' => 'all',
//			'toolbar' => 'full',
//			'media_upload' => 1,
//		),
		array (
			'key' => 'field_as8f4065eafcb',
			'label' => 'Plik do pobrania',
			'name' => 'download_inter',
			'type' => 'file',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'library' => 'all',
			'min_size' => '',
			'max_size' => '',
			'mime_types' => '',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'interpelations',
			),
		),
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'publications',
			),
		),
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'protocols',
			),
		),
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'resolutions',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;