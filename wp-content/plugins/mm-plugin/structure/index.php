<?php
/*** Post types ***/
//require 'post-type/testimonial.php';
//require 'post-type/services.php';
require 'post-type/projekt-umwetu.php';
require 'post-type/materialy-filmowe.php';
require 'post-type/list_councilors.php';
require 'post-type/galeria.php';
require 'post-type/interpelacje.php';
require 'post-type/news.php';
require 'post-type/publikacje.php';
require 'post-type/fromwork.php';
require 'post-type/projekty-uchwal.php';
require 'post-type/elections.php';

/*** Taxonomies ***/
//require 'taxonomy/';

/*** Fields ***/
require 'custom-field/okr.php';
require 'custom-field/councilors.php';
require 'custom-field/home.php';
require 'custom-field/about-us.php';
require 'custom-field/umwetu.php';
require 'custom-field/video.php';
require 'custom-field/single_video.php';
require 'custom-field/galeria.php';
require 'custom-field/interpelacje.php';
require 'custom-field/interpelacje_post_type.php';
require 'custom-field/materials.php';
require 'custom-field/materials-2010.php';
//require 'custom-field/news.php';
//require 'custom-field/forms.php';
//require 'custom-field/contact.php';
//require 'custom-field/default.php';

require 'settings/website.php';
