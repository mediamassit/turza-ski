<?php
/**
 * Plugin Name: First Image Remover
 * Description: Remove the first image from the post content
 * Plugin URI:  http://wordpress.stackexchange.com/a/142494/26350 
 * Version:     0.0.1
 */

/**
 * Create the 'First Image Remover' admin menu
 */

function wpse_142494_create_menu()
{
    // Create new top-level menu:
    add_menu_page( 
        'First Image Remover', 
        'First Image Remover', 
        'manage_options', 
        'wpse_142494_settings_page', 
        'wpse_142494_settings_page'
    );
}

add_action('admin_menu', 'wpse_142494_create_menu');

/**
 * Create the 'Image Replacer' settings pge
 */
function wpse_142494_settings_page()
{ 
?>
    <div class="wrap">
        <h2>First Image Remover</h2>
        <p>Remove the first image of each post in the selected loop.</p>


        <h3>Help:</h3>
        <p>Avialable GET parameters: 
        <pre>
wpse_ppp - Posts Per Page (int), 
wpse_offset - Offset (int),
wpse_update - Update mode (boolean) 

Update Example for 5 posts with offset 10:

/wp-admin/admin.php?page=wpse_142494_settings_page&wpse_offset=10&wpse_ppp=5&wpse_update=yes
        </pre>
        <h3>Loop:</h3>
        <?php wpse_142494_loop(); ?>
    </div>
<?php
}

/**
 * Fetch posts based on user input
 */
function wpse_142494_loop()
{
    // Only site admin can update posts:
    if( ! current_user_can( 'manage_options' ) ) return;

    // Get user input:
    $params = filter_input_array( INPUT_GET, array( 
        'wpse_offset'  => FILTER_SANITIZE_NUMBER_INT,
        'wpse_ppp'     => FILTER_SANITIZE_NUMBER_INT, 
        'wpse_update'  => FILTER_VALIDATE_BOOLEAN, 
    ) );

    // Fetch posts to update:   
    $posts = get_posts( array( 
        'post_type'      => 'post', 
        'posts_per_page' => ( ! empty( $params['wpse_ppp'] ) )    ? $params['wpse_ppp']    : 10 , 
        'offset'         => ( ! empty( $params['wpse_offset'] ) ) ? $params['wpse_offset'] : 0, 
    ) );

    // Loop through posts:
    $li = '';
    foreach( $posts as $post ):

        if( $params['wpse_update'] ):           
            // Update each post with your reg-ex content filter:
            $pid = wp_update_post( array( 
                'ID'           => $post->ID,
                'post_content' => preg_replace( "/<img[^>]+\>/i", "", $post->post_content, 1 )
            ) );

            // Show the update process:
            $li .= sprintf( '<li>%d - <strong>%s</strong> - was %s updated</li>', 
                $post->ID, 
                $post->post_title,
                ( 0 < $pid ) ? '' : 'NOT' 
            );
        else:
            // Show the post list that will be updated
            $li .= sprintf( '<li>%d - <strong>%s</strong> - will be updated</li>', 
                $post->ID,
                $post->post_title
            );
        endif;

    endforeach;

    // Output:
    printf( '<strong>Settings:</strong> Posts: %d - Offset: %d - Update: %s <ul>%s</ul>', 
        $params['wpse_ppp'], 
        $params['wpse_offset'], 
        $params['wpse_update'] ? 'ON' : 'OFF', 
        $li 
    );
}