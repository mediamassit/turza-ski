<?php get_header() ;?>
<section class="bg">
    <div class="slider-bg-blog">
    </div>
    <div class="slider-bg-1-blog">
  </div>

    <div class="title-slider">
        <h2>KRZYSZTOF TURZAŃSKI <br> <span class="letter">SŁAWA UMIŃSKA-DURAJ</span></h2>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis_07.png" alt="Krzysztof Turzański" class="krzysztof">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis-slawa_03.png" alt="Sława Umińska-Duraj" class="slawa">
    </div>
</section>

<main id="page-main">
    <div class="row">
        <div class="container">
            <div class="list-post col-md-9 col-sm-8 col-xs-12">
                <div class="wrapper-okr">
                    <h3 class="title-subpage"><?php _e('ZŁOŻONE WNIOSKI I INTERPELACJE', THEME_NAME) ;?> </h3>
                    <div class="wrapper-conclusion">
                        <p class="inter-title"><?php echo the_title() ;?> </p>
                        <p><?php echo the_content() ;?></p>
                        <?php
                        $file = get_field('download_inter') ;
                        if($file):
                        ;?>
                      <a class="hoverpdf" href="<?php echo $file['url'];?>" download><img src="<?php echo get_template_directory_uri(); ?>/assets/images/download_10.png" alt="">POBIERZ PLIK</a>
                      <?php endif;?>
                    </div>
                </div>

                </div>

            </div>
        </div>
    </div>
</main>

<?php get_footer() ;?>
