<?php

get_header();

?>
<section class="bg">
    <div class="slider-bg-blog">
    </div>
    <div class="slider-bg-1-blog">
    </div>

    <div class="title-slider">
        <h2>KRZYSZTOF TURZAŃSKI <br> <span class="letter">SŁAWA UMIŃSKA-DURAJ</span></h2>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis_07.png" alt="Krzysztof Turzański" class="krzysztof">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis-slawa_03.png" alt="Sława Umińska-Duraj" class="slawa">
    </div>
</section>
<main id="page-main">
    <div class="container">
        <div class="row">
            <div class="list-post col-md-9 col-sm-8 col-xs-12">
                <h3><?php echo get_the_title(); ?></h3>
                <p><?php echo the_content(); ?></p>
            </div>
            <div class="sidebar col-md-3 col-sm-4 col-xs-12">
                <?php
                  if ( is_active_sidebar( 'sidebar_default' ) ) {
                    dynamic_sidebar( 'sidebar_default' );
                  }
                ?>

                <div class="social">
                  <ul>
                    <?php $socials = get_field('social_group',option);
              					if($socials):
                        foreach($socials as $social):
                      ;?>
                    <li class="item" style="background-image: url(<?php echo $social['social_icon']; ?>)"><a href="<?php echo $social['social_link']; ?>"></a></li>
                    <?php endforeach; endif ;?>
                  </ul>
                </div>


          </div>
        </div>
      </div>
</main>
<?php get_footer(); ?>
