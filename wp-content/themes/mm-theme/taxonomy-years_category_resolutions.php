<?php get_header() ;?>

<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;


?>

 <section class="bg">
    <div class="slider-bg-blog">
    </div>
    <div class="slider-bg-1-blog">
    </div>

    <div class="title-slider">
        <h2>KRZYSZTOF TURZAŃSKI <br> <span class="letter">SŁAWA UMIŃSKA-DURAJ</span></h2>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis_07.png" alt="Krzysztof Turzański" class="krzysztof">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis-slawa_03.png" alt="Sława Umińska-Duraj" class="slawa">
    </div>
</section>
<main id="page-main">
    <div class="row">

        <div class="container">

            <div class="list-post col-md-9 col-sm-8 col-xs-12">
							<div class="container margin-btm">
					 		 <h3 class="title-subpage">PROJEKTY UCHWAŁ</h3>
					         <?php
					         $taxonomy = get_the_taxonomies(get_the_id());
					 				$id = get_queried_object()->term_id;
					         foreach($taxonomy as $i => $val) {
					           $taxonomy = $i;
					         }
					         display_tabs_inter($taxonomy,$id);
					 					; ?>
					 	</div>
              <?php
                  $args = array(
                    'p' => $pinned->ID,
                    'post_type' => 'projekty_uchwal',
                    'paged' => $paged
                  );?>

                <?php  if (have_posts() ) : while ( have_posts() ) : the_post();?>
                  <?php get_template_part( 'content' ); ?>
                <?php  endwhile ; endif?>
                  <div class="pagi">
                  <?php
                    $pagination = get_paginate_links(array(
                      'format' => 'page/%#%',
                      'current' => max(1, get_query_var('paged')),
                      'total' =>  $wp_query->max_num_pages,
                    ));
                    echo get_template_part_args('partials/pagination', array('pagination' => $pagination, 'max_pages' =>  $wp_query->max_num_pages));
                  ?>
                  </div>

                </div>
                <div class="sidebar col-md-3 col-sm-4 col-xs-12">
                  <?php
                    if ( is_active_sidebar( 'sidebar_wyszukiwarka' ) ) {
                      dynamic_sidebar( 'sidebar_wyszukiwarka' );
                    }
                  ?>
									<?php
                  if ( is_active_sidebar( 'sidebar_about' ) ) {
                    dynamic_sidebar( 'sidebar_about' );
                  }
                  ?>
                    <div class="social">
                      <ul>
                        <?php $socials = get_field('social_group',option);
                if($socials):

                            foreach($socials as $social):

                          ;?>
                        <li class="item" style="background-image: url(<?php echo $social['social_icon']; ?>)"><a href="<?php echo $social['social_link']; ?>"></a></li>
                        <?php endforeach; endif ;?>
                      </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer() ;?>
