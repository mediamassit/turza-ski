<?php

get_header();

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
  'post_type' => 'news',
  'posts_per_page' => 8,
  'paged' => $paged
);
$loop = new WP_Query( $args );

?>


<section class="bg">
	<div class="slider-bg-blog"></div>
	<div class="slider-bg-1-blog"></div>
	<div class="title-slider">
		<h2>KRZYSZTOF TURZAŃSKI <br> <span class="letter">SŁAWA UMIŃSKA-DURAJ</span></h2>
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis_07.png" alt="Krzysztof Turzański" class="krzysztof">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis-slawa_03.png" alt="Sława Umińska-Duraj" class="slawa">
	</div>
</section>
<main>
	<div class="container">
		<div class="row">
			<div class="list-post col-md-9 col-sm-8 col-xs-12">
        <?php $desc_news = get_field('desc_news',option); $title_news = get_field('title_news',option); ?>
        <h3 class="title-subpage"> <?php echo $title_news; ?> </h3>
        <p class="desc-subpage space"><?php echo $desc_news; ?></p>
				<?php  while ( $loop->have_posts() ) : $loop->the_post(); ?>
					<?php get_template_part( 'content' ); ?>
				<?php endwhile ;?>
				<div class="pagi">
					<?php
					$pagination = get_paginate_links(array(
						'format' => 'page/%#%',
						'current' => max(1, get_query_var('paged')),
						'total' => $loop->max_num_pages,
					));
					echo get_template_part_args('partials/pagination', array('pagination' => $pagination, 'max_pages' => $loop->max_num_pages));
					?>
				</div>
			</div>
      <div class="sidebar col-md-3 col-sm-4 col-xs-12">
      <?php
      if ( is_active_sidebar( 'sidebar_about' ) ) {
        dynamic_sidebar( 'sidebar_about' );
      }
      ?>
        <div class="social">
          <ul>
            <?php $socials = get_field('social_group',option);
    if($socials):

                foreach($socials as $social):

              ;?>
            <li class="item" style="background-image: url(<?php echo $social['social_icon']; ?>)"><a href="<?php echo $social['social_link']; ?>"></a></li>
            <?php endforeach; endif ;?>
          </ul>
        </div>
      </div>
		</div>
	</div>
</main>

<?php get_footer(); ?>
