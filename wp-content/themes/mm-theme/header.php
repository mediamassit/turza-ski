
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset') ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php bloginfo('name'); ?></title>
	<link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/images/img_03.png">
	<!--[if lt IE 9]>
	<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<!-- ... build:remove -->
  <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700&amp;subset=latin-ext" rel="stylesheet">
  <script type="text/javascript">
	window.onload = function(){
	var el = document.getElementById('load');
	el.style.display = 'none';
	};
   </script>
	<?php wp_head(); ?>
</head>


<body>
	<div id="load">
    </div>

  <?php $logo = get_field('logo_header'); ?>
	<div id="page">
        <header>
            <div class="container">
							<div class="wrapper-header">
									<a href="<?php echo home_url(); ?>">
											<h1 class="logo">Krzysztof Turzański - Sława Umińska-Duraj</h1>
									</a>
									<nav>
										<ul id="desktop">
											<?php
											wp_nav_menu(
													array(
															'container'            => "",
															'container_class'    => "",
															'container_id'        => "",
															'fallback_cb'        => false,
															'menu_class'        => "",
															'before'            => '',
															'after'             => '<span class="slash">/</span>',
															'link_before'        => '',
															'link_after'        => '',
															'items_wrap'        => '%3$s',
															'theme_location'    => "primary",
															'walker'            => '',
													)
											);
									?>
											</ul>
									</nav>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/hamburger-menu_06.png" alt="MENU" class="hamburger">
							</div>
            </div>

            <ul id="mobile" style="opacity: 1;">
                <?php
                    wp_nav_menu(
                        array(
                            'container'            => "",
                            'container_class'    => "",
                            'container_id'        => "",
                            'fallback_cb'        => false,
                            'menu_class'        => "",
                            'before'            => '',
                            'after'                => '',
                            'link_before'        => '',
                            'link_after'        => '',
                            'items_wrap'        => '%3$s',
                            'theme_location'    => "primary",
                            'walker'            => '',
                        )
                    );
                ?>
            </ul>

        </header>
