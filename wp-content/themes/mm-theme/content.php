<?php

$categories = get_the_category();
$author = get_field('post_author_name');
$comments_count = wp_count_comments(get_the_ID());
$comments_count = $comments_count->total_comments;
?>
<div class="single-post col-md-6 col-sm-12 col-xs-12">
	<div class="<?php if (is_page_template('page-templates/template-videos.php')) { echo "wrapper-image-vid"; } else { echo "wrapper-image"; } ?>">
	  <a href="<?php the_permalink();?>"><img src="<?php echo get_the_post_thumbnail_url(get_the_id(),'thumb-420x320'); ?>"></a>
	</div>
	<?php $title = get_the_title();
				$title_cut = substr($title, 0, 45);
				if(strlen($title) > 45) { $title_cut .= "..."; } ?>
	<div class="description-post">
		<p class="date"><?php echo get_the_time('d-m-Y', $post->ID); ?></p>
		<a href="<?php the_permalink();?>"><h2><?php echo $title_cut ?></h2></a>
		<p><?php echo wp_trim_words( get_the_content(), 20, '...' ); ?></p>
		<p><a href="<?php the_permalink();?>">Czytaj więcej</a></p>
	</div>
	<p class="comments"><a href="<?php the_permalink(); ?>#comments"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/comment_07.png" alt=""><span class="number-comment"><?php if($comments_count!=0) echo $comments_count.' '; ?></span> <?php echo comments_number( 'Brak komentarzy, bądź pierwszy!', 'komentarz', 'komentarze' ); ?></a></p>
</div>
<?php /*
<figure class="featured-image bg-light-silver">
	<?php
		if(has_post_thumbnail()) :
			$featured_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
	?>
		<a href="<?php echo get_permalink(); ?>"><img src="<?php echo $featured_image[0]; ?>" alt="<?php the_title(); ?>"/></a>
	<?php endif; ?>

	<figcaption>
		<ul class="blog-post-meta">
			<li><i class="icon icon-clock"></i> <?php printf(__('%sr. o godz: %s', 'dmx-theme'), get_the_time(__('j M Y', 'dmx-theme')), get_the_time(__('G:i', 'dmx-theme'))); ?></li>
			<?php if($author) : ?><li><i class="icon icon-user"></i> <?php _e('Ekspert:', 'dmx-theme'); ?> <?php echo $author; ?></li><?php endif; ?>
			<li><i class="icon icon-flag"></i> <?php _n('Kategoria:', 'Kategorie:', sizeof($categories), 'dmx-theme'); ?>
			<?php foreach($categories as $category) : ?><a href="<?php echo get_category_link($category->term_id); ?>"><?php echo esc_attr($category->name); ?></a> <?php endforeach; ?>
			</li>
		</ul>
		<h2 class="mb-sm text-uppercase"><a href="<?php echo esc_url(get_permalink()); ?>"><?php the_title(); ?></a></h2>
		<?php if($post->post_excerpt): ?>
			<?php the_excerpt(); ?>
		<?php else: ?>
		<p class="text-dark-gray"><?php echo wp_trim_words(get_the_content(), 22, '...'); ?></p>
		<?php endif; ?>
		<p class="mb-sm"><a href="<?php the_permalink(); ?>" class="btn btn-border-gradient text-wsb"><span><?php _e('Czytaj więcej', 'dmx-theme'); ?></span></a></p>
	</figcaption>
</figure>*/ ?>
