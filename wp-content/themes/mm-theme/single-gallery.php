<?php

get_header();

?>


<section class="bg">
    <div class="slider-bg-blog">
    </div>
    <div class="slider-bg-1-blog">
   </div>

    <div class="title-slider">
        <h2>KRZYSZTOF TURZAŃSKI <br> <span class="letter">SŁAWA UMIŃSKA-DURAJ</span></h2>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis_07.png" alt="Krzysztof Turzański" class="krzysztof">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis-slawa_03.png" alt="Sława Umińska-Duraj" class="slawa">
    </div>
</section>

<main>
    <div class="container">
        <div class="row">
            <div class="list-post col-md-9 col-sm-8 col-xs-12">
                <div class="main-post-full">
                    <a href="<?php echo get_the_post_thumbnail_url(get_the_id(),'full'); ?>"><img class="post-thumbnail" src="<?php echo get_the_post_thumbnail_url(get_the_id(),'thumb-870x415'); ?>" alt=""></a>
                    <div class="description-post-full">
                        <p class="date"><?php echo get_the_time('Y-m-d', $post->ID); ?></p>
                        <h2><?php the_title(); ?></h2>
                        <?php the_post(); ;?>
                        <?php echo the_content(); ?>

                    </div>
                </div>

          			<?php $items = get_field('gallery');
          			if($items): ;?>
          			<div class="gallery">
          				<div class="popup-gallery">
          					<div class="row">
          						<?php foreach($items as $item): ;?>
          							<div class="item-photo col-md-3 col-sm-6 col-xs-12">
          								<a href="<?php echo $item['url']; ?>" >
          									<img src="<?php echo $item['sizes']["thumb-175x115"]; ?>" alt="" style="margin-top: 25px;">
          								</a>
          							</div>
          						<?php endforeach ?>
          					</div>
          				</div>
          			</div>
          			<?php endif ?>
                <div class="pagi-post">
                  <div class="nav-prev">
                    <?php previous_post_link(); ?>
                  </div>
                  <div class="nav-next">
                    <?php next_post_link(); ?>
                  </div>
                </div>
                <div class="comments" id="comments">
                  <h3><?php _e('Komentarze',THEME_NAME); ?></h3>
                  <hr/>
                  <?php echo do_shortcode('[vivafbcomment]'); ?>
                  <hr/>
                  <?php comments_template(); ?>
                </div>
            </div>
            <div class="sidebar col-md-3 col-sm-4 col-xs-12">
              <?php
                if ( is_active_sidebar( 'sidebar_wyszukiwarka' ) ) {
                  dynamic_sidebar( 'sidebar_wyszukiwarka' );
                }
              ?>
                     
					<?php 
					  	if ( is_active_sidebar( 'sidebar_about' ) ) {
                        dynamic_sidebar( 'sidebar_about' );}
					?>
        				<div class="social">
        					<ul>
        					<?php $socials = get_field('social_group',option);
        						if($socials):
        						foreach($socials as $social): ;?>
        						      <li class="item" style="background-image: url(<?php echo $social['social_icon']; ?>)"><a href="<?php echo $social['social_link']; ?>"></a></li>
        					  <?php endforeach; endif ;?>
        					</ul>
        				</div>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>
