<?php get_header();

/**
 * Template Name: Interpelacje
 */
?>

<?php

$args = array(
 'post_type' => 'interpelations'
);
$loop = new WP_Query( $args );
?>

 <section class="bg">
    <div class="slider-bg-blog">
    </div>
    <div class="slider-bg-1-blog">
    </div>

    <div class="title-slider">
        <h2>KRZYSZTOF TURZAŃSKI <br> <span class="letter">SŁAWA UMIŃSKA-DURAJ</span></h2>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis_07.png" alt="Krzysztof Turzański" class="krzysztof">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis-slawa_03.png" alt="Sława Umińska-Duraj" class="slawa">
    </div>
</section>
<main id="page-main">
    <div class="row">
        <div class="container">
            <div class="list-post col-md-9 col-sm-8 col-xs-12">
                <div class="wrapper-okr">
                  <?php $title = get_field('title_interpelations');
                        $desc = get_field('desc_interpelations'); ?>
                    <h3 class="title-subpage"><?php echo $title; ?></h3>
                    <p class="desc-subpage"><?php echo $desc; ?></p>
                    <?php
                      $categories = get_terms( 'years_category', array(
                        'orderby'    => 'count',
                        'hide_empty' => true,
                      ) );
                    if($categories):
                    ;?>
                    <div style="margin-left:15px;">
                       <?php foreach($categories as $category):
              						$tax= $category->taxonomy;
              						$cat= $category->name
              							/*  */
              						;?>
                            <a href="<?php echo get_site_url()."/".$tax."/".$cat; ;?>" class="date-change" ><?php echo $category->name ;?></a>
                          <?php endforeach ;?>
                    </div>
                    <?php endif ;?>
                  </div>
                </div>
              <div class="sidebar col-md-3 col-sm-4 col-xs-12">
                  <?php
                  if ( is_active_sidebar( 'sidebar_about' ) ) {
                    dynamic_sidebar( 'sidebar_about' );
                  }
                  ?>
                  <div class="social">
                    <ul>
                      <?php $socials = get_field('social_group',option);
    					            if($socials):
                          foreach($socials as $social):

                        ;?>
                        <li class="item" style="background-image: url(<?php echo $social['social_icon']; ?>)"><a href="<?php echo $social['social_link']; ?>"></a></li>
                      <?php endforeach; endif ;?>
                    </ul>
                  </div>
              </div>
          </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>
