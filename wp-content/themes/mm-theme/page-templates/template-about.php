<?php get_header();

/**
 * Template Name: O nas
 */
?>

<section class="bg">
    <div class="slider-bg-blog">
    </div>
    <div class="slider-bg-1-blog">
    </div>

    <div class="title-slider">
        <h2>KRZYSZTOF TURZAŃSKI <br> <span class="letter">SŁAWA UMIŃSKA-DURAJ</span></h2>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis_07.png" alt="Krzysztof Turzański" class="krzysztof">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis-slawa_03.png" alt="Sława Umińska-Duraj" class="slawa">
    </div>
</section>

<?php $title_about = get_field('title_about');
      $content_about = get_field('content_about');
?>

<main id="aboutme">
    <div class="container">
        <div class="background-about">
            <div class="white-about">
                <div class="wrapper-about-text">
                    <h3><?php echo $title_about; ?></h3>
                    <?php echo the_content(); ?>
                </div>
            </div>
        </div>
    </div>
   <div class="container">
        <div class="row">
            <div class="about-content col-md-9 col-sm-8 col-xs-12">
				<h3>SŁAWA UMIŃSKA DURAJ <span class="small-title">- najważniejsze informacje</span></h3>
                <div class="scroll-timeline mCustomScrollbar" data-mcs-theme="rounded-dots-dark">
                  <div class="wrapper-timeline">
                    <?php $points = get_field('slawa_history');

                      foreach($points as $point_):

                    ;?>
                    <div class="point">
                        <div class="date"><?php echo $point_['date_history']; ?></div>
                        <p class="description-point">
                            <?php echo $point_['content_history']; ?>
                        </p>
                    </div>
                  <?php endforeach ;?>
                </div>
              </div>
				<h3>KRZYSZTOF TURZAŃSKI <span class="small-title">- najważniejsze informacje</span></h3>
              <div class="scroll-timeline mCustomScrollbar" data-mcs-theme="rounded-dots-dark">
                <div class="wrapper-timeline">
                    <?php $history = get_field('turzanski_history');

                      foreach($history as $point):

                    ;?>
                    <div class="point">
                        <div class="date"><?php echo $point['date_history']; ?></div>
                        <p class="description-point">
                            <?php echo $point['content_history']; ?>
                        </p>
                    </div>
                  <?php endforeach ;?>
                </div>
              </div>
            </div>
            <div class="sidebar col-md-3 col-sm-4 col-xs-12">
            <?php
						if ( is_active_sidebar( 'sidebar_about' ) ) {
							dynamic_sidebar( 'sidebar_about' );
						}
				    ?>
              <div class="social">
                <ul>
                  <?php $socials = get_field('social_group',option);
					if($socials):

                      foreach($socials as $social):

                    ;?>
                  <li class="item" style="background-image: url(<?php echo $social['social_icon']; ?>)"><a href="<?php echo $social['social_link']; ?>"></a></li>
                  <?php endforeach; endif ;?>
                </ul>
              </div>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>
