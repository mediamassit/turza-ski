<?php get_header(); ?>

<?php
/**
 * Template Name: Strona główna
 */
?>
<div class="container-max">
    <section class="bg">
        <div class="slider-bg">
        </div>
        <div class="slider-bg-1">
        </div>

        <div class="title-slider">
            <h2>KRZYSZTOF TURZAŃSKI <br> <span class="letter">SŁAWA UMIŃSKA-DURAJ</span></h2>
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis_07.png" alt="Krzysztof Turzański" class="krzysztof">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis-slawa_03.png" alt="Sława Umińska-Duraj" class="slawa">
        </div>
          <div class="list-box">
              <?php $boxs = get_field('list_box');
                $count = 1;
                foreach($boxs as $box):
                $licznik
              ;?>
            <a href="<?php echo $box['link_box'] ?>">
                <article class="box <?php if($count == 2) {echo "about";} elseif($count == 4 || $count == 6) {echo "select";} else { echo "blog";}?>" style="background-image: url(<?php echo $box['image_box'];?>)">
                    <div class="overlay" style="background-color: rgba(<?php $color = $box['color_box'];
                    $resultColor = hex2rgb($color); echo $resultColor; echo ',0.8' ?>);">
                        <div class="title <?php $licznik += 1; if($licznik%2==0)  echo 'white';  ?>">
                            <h2>
                              <?php echo $box['box_title'] ;?></h2>
                            <p><?php echo $box['desc_box'] ;?></p>
                        </div>
                    </div>
                </article>
            </a>
            <?php $count++; ?>
            <?php endforeach ;?>
        </div>
    </section>
</div>

<div class="social">
    <ul>
		<?php $socials = get_field('social_group',option);
		 if($socials):

			foreach($socials as $social):

		  ;?>
		<li class="item" style="background-image: url('<?php echo $social['social_icon']; ?>')"><a href="<?php echo $social['social_link']; ?>"></a></li>
		<?php endforeach ; endif; ?>
    </ul>
</div>


<?php get_footer(); ?>
