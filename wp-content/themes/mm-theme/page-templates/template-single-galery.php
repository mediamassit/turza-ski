<?php

get_header();

?>


 <section class="bg">
    <div class="slider-bg-blog">
    </div>
    <div class="slider-bg-1-blog">  
    </div>

    <div class="title-slider">
        <h2>KRZYSZTOF TURZAŃSKI <br> <span class="letter">SŁAWA UMIŃSKA-DURAJ</span></h2>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis_07.png" alt="Krzysztof Turzański" class="krzysztof">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis-slawa_03.png" alt="Sława Umińska-Duraj" class="slawa">
    </div>
</section>

<main>
    <div class="container">
        <div class="row">
            <div class="list-post col-md-9 col-sm-8 col-xs-12">
                <div class="main-post-full">
                    <img class="post-thumbnail" src="<?php echo get_the_post_thumbnail_url(get_the_id(),'thumb-868x414'); ?>" alt="">
                    <div class="description-post-full">
                        <p class="date"><?php echo get_the_time('d-m-Y', $post->ID); ?></p>
                        <h2><?php the_title(); ?></h2>
                        <?php the_post(); ;?>
                        <p><?php echo the_content(); ?></p>
                        
                    </div>
                </div>
                <div class="pagi-post">
                    <a href="#"><div class="nav-prev"></div></a>
                    <a href="#"><div class="nav-next"></div></a>
                </div>
                <?php $boxs = get_field('galeria');
                  foreach($boxs as $box):
                ;?>
              <img src="<?php echo $box['obrazek']; ?>" alt="">  
              <?php endforeach ?> 
          </div>
            <div class="sidebar col-md-3 col-sm-4 col-xs-12">
              <?php
                if ( is_active_sidebar( 'sidebar_wyszukiwarka' ) ) {
                  dynamic_sidebar( 'sidebar_wyszukiwarka' );
                } 
              ?>
                    <?php display_categories(get_the_id()); ?>
                    <?php
                if ( is_active_sidebar( 'sidebar_blog' ) ) {
                  dynamic_sidebar( 'sidebar_blog' );
                } 
              ?>
            
          <ul>
			<?php if($socials): ?>
            <?php $socials = get_field('social_group');

                foreach($socials as $social):

              ;?>
            <li class="item" style="background-image: url(<?php echo $social['social_icon']; ?>)"><a href="<?php echo $social['social_link']; ?>"></a></li>
            <?php endforeach ; endif; ?>
          </ul>
            
        </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>
