<?php get_header();

/**
 * Template Name: OKR + lista
 */
?>

<section class="bg">
    <div class="slider-bg-blog">
    </div>
    <div class="slider-bg-1-blog">
    </div>

    <div class="title-slider">
        <h2>KRZYSZTOF TURZAŃSKI <br> <span class="letter">SŁAWA UMIŃSKA-DURAJ</span></h2>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis_07.png" alt="Krzysztof Turzański" class="krzysztof">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis-slawa_03.png" alt="Sława Umińska-Duraj" class="slawa">
    </div>
</section>

<main id="page-main">
    <div class="row">
        <div class="container">
            <div class="list-post col-md-9 col-sm-8 col-xs-12">
                <div class="wrapper-okr">
                      <?php $title = get_field('article_title'); ?>
                      <h3 class="title-subpage"><?php echo $title ;?></h3>
                       <?php echo the_content(); ?>

                </div>

                <div class="wrapper-zone">
                  <?php $district = get_field('add_district');
                    if($district):
                    foreach($district as $single):
                  ?>
                  <?php $person = $single['person'];
                    if($person): ;?>
                            <div class="zone">
                                <h3><?php echo $single['title'] ;?></h3>

                                <ul>
                        <?php
                        foreach($person as $single_person):?>
                                    <li><?php echo $single_person['name_surname'];?></li>
                                      <?php endforeach;?>
                                </ul>
                            </div>
                            <?php
                    endif;
                    endforeach;
                    endif;
                  ?>
                </div>
                <div class="list">
                    <h3 class="title-list"><?php _e('SPIS RADNYCH:', THEME_NAME) ;?></h3>
                    <?php

                      $args = array(
                          'post_type' => 'councilors',
                        );

                        $the_query = new WP_Query( $args );
                      ?>
                        <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                        $name = get_field('name_councilor');
                        $short_desc = get_field('short_desc');
                        $phone = get_field('phone_councilor');
                        $mail = get_field('mail_councilor');
                        $photo = get_field('photo_councilor');
                        $conclusions = get_field('conclusions');?>
                        <?php
                        $count = 0;
                        $cat = get_field('check_person');

                        $args2 = array(
                          'post_type' => 'interpelations',
							'posts_per_page' => 1,
                           'tax_query' => array(
                            array(
                                 array(
                                'taxonomy' => 'radni_category',
                                'field'    => 'slug',
                                'terms'    => $cat
                               ),
                            ),
                          ),
                        );
                        $query = new WP_Query( $args2 );
                      ?>
                    <div class="line">
                        <div class="av"><a href="<?php the_permalink();?>"><img src="<?php echo $photo['sizes']['thumb-220x190'] ;?>" alt=""></a></div>
                        <div class="personal">
                            <a href="<?php the_permalink();?>"><h3><?php echo $name ;?></h3></a>
                            <h4><?php _e('Licznik wniosków i interpelacji:', THEME_NAME) ;?> <?php echo $query ->found_posts; ?></h4>
                            <?php echo $short_desc ;?>
                            <h4><?php _e('Kontakt:', THEME_NAME) ;?></h4>
                            <p class="contact"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/phone_03.png" alt=""><a href="tel:<?php echo $phone ;?>"><?php echo $phone; ?></a></p>
                            <p class="contact"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/email_06.jpg" alt=""><a href="mailto:<?php echo $mail ;?>"><?php echo $mail; ?></a></p>
                        </div>
                    </div>
                    <?php endwhile; endif ;?>

                </div>

            </div>
               <div class="sidebar col-md-3 col-sm-4 col-xs-12">
                  <?php
                  if ( is_active_sidebar( 'sidebar_about' ) ) {
                    dynamic_sidebar( 'sidebar_about' );
                  }
                  ?>
              <div class="social">
                <ul>
                  <?php $socials = get_field('social_group',option);
					if($socials):
                      foreach($socials as $social):

                    ;?>
                  <li class="item" style="background-image: url(<?php echo $social['social_icon']; ?>)"><a href="<?php echo $social['social_link']; ?>"></a></li>
                  <?php endforeach; endif ;?>
                </ul>
              </div>
              </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>
