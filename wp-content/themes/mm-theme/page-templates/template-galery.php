<?php get_header();

/**
 * Template Name: Galeria
 */
?>

<?php

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
	'post_type' => 'gallery',
	'posts_per_page' => 8,
	'paged' => $paged
);
$loop = new WP_Query( $args );

?>



 <section class="bg">
    <div class="slider-bg-blog">
    </div>
    <div class="slider-bg-1-blog">
    </div>

    <div class="title-slider">
        <h2>KRZYSZTOF TURZAŃSKI <br> <span class="letter">SŁAWA UMIŃSKA-DURAJ</span></h2>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis_07.png" alt="Krzysztof Turzański" class="krzysztof">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis-slawa_03.png" alt="Sława Umińska-Duraj" class="slawa">
    </div>
</section>

<main id="page-main">
    <div class="container">
        <div class="row">
            <div class="list-post col-md-9 col-sm-8 col-xs-12">
              <?php $title_material = get_field('title_materials'); $desc_material = get_field('desc_materials'); ?>
                <h3 class="title-subpage"><?php echo $title_material; ?></h3>
                <p class="desc-subpage"><?php echo $desc_material; ?></p>
                <div class="row">
                  <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

                <?php get_template_part( 'content' ); ?>
				<?php endwhile ;?>

                </div>
              <div class="pagi">
                <?php
                  $pagination = get_paginate_links(array(
                    'format' => 'page/%#%',
                    'current' => max(1, get_query_var('paged')),
                    'total' => $loop->max_num_pages,
                  ));
                  echo get_template_part_args('partials/pagination', array('pagination' => $pagination, 'max_pages' => $loop->max_num_pages));
                ?>
                </div>
            </div>
            <div class="sidebar col-md-3 col-sm-4 col-xs-12">
              <?php
              if ( is_active_sidebar( 'sidebar_about' ) ) {
                dynamic_sidebar( 'sidebar_about' );
              }
              ?>
							<div class="social">
                <ul>
                  <?php $socials = get_field('social_group',option);
									if($socials):
                      foreach($socials as $social):

                    ;?>
                  <li class="item" style="background-image: url(<?php echo $social['social_icon']; ?>)"><a href="<?php echo $social['social_link']; ?>"></a></li>
                  <?php endforeach; endif ;?>
                </ul>
              </div>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>
