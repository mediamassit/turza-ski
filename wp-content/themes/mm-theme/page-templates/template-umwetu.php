<?php get_header();

/**
 * Template Name: Projekt UMWETU
 */
?>

<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
	'post_type' => 'project',
	'posts_per_page' => 8,
	'paged' => $paged
);
$loop = new WP_Query( $args );
?>



 <section class="bg">
    <div class="slider-bg-blog">
    </div>
    <div class="slider-bg-1-blog"></div>

    <div class="title-slider">
        <h2>KRZYSZTOF TURZAŃSKI <br> <span class="letter">SŁAWA UMIŃSKA-DURAJ</span></h2>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis_07.png" alt="Krzysztof Turzański" class="krzysztof">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis-slawa_03.png" alt="Sława Umińska-Duraj" class="slawa">
    </div>
</section>

<main id="page-main">
    <div class="container">
        <div class="row">
            <div class="list-post col-md-9 col-sm-8 col-xs-12">
              <?php $title_umwetu = get_field('title_umwetu'); $desc_umwetu = get_field('desc_umwetu'); $title_umwetu1 = get_field('title_umwetu1'); $desc_umwetu1 = get_field('desc_umwetu1'); ?>
								<?php if($paged == 1): ?>
									<h3 class="title-subpage blue"><?php echo $title_umwetu; ?></h3>
	                <p class="desc-subpage border-desc" style="color: #03346f;"><?php echo $desc_umwetu; ?></p>
								<?php endif; ?>
                <div class="row">
                  <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                  	<?php get_template_part( 'content' ); ?>
                  <?php endwhile ;?>

                </div>
                <div class="pagi">
                  <?php
                    $pagination = get_paginate_links(array(
                      'format' => 'page/%#%',
                      'current' => max(1, get_query_var('paged')),
                      'total' => $loop->max_num_pages,
                    ));
                    echo get_template_part_args('partials/pagination', array('pagination' => $pagination, 'max_pages' => $loop->max_num_pages));
                  ?>
                </div>
            </div>
          <div class="sidebar col-md-3 col-sm-4 col-xs-12">
				<?php display_custom_category('kategorie_category');?>
                <?php
                if ( is_active_sidebar( 'sidebar_umwetu' ) ) {
                  dynamic_sidebar( 'sidebar_umwetu' );
                }
                ?>
              <div class="social">
                <ul>
                  <?php $socials = get_field('social_group',option);
					if($socials):
                      foreach($socials as $social):
                    ;?>
                  <li class="item" style="background-image: url(<?php echo $social['social_icon']; ?>)"><a href="<?php echo $social['social_link']; ?>"></a></li>
                  <?php endforeach; endif ;?>
                </ul>
              </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>
