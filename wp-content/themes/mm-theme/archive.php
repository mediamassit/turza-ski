<?php

get_header();

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;


$short_content = get_field('short_content');
?>


<section class="bg">
    <div class="slider-bg-blog">
    </div>
    <div class="slider-bg-1-blog">
    </div>

    <div class="title-slider">
        <h2>KRZYSZTOF TURZAŃSKI <br> <span class="letter">SŁAWA UMIŃSKA-DURAJ</span></h2>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis_07.png" alt="Krzysztof Turzański" class="krzysztof">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis-slawa_03.png" alt="Sława Umińska-Duraj" class="slawa">
    </div>
</section>
<main>
    <div class="container">
        <div class="row">
            <div class="list-post col-md-9 col-sm-8 col-xs-12">
				<h3 class="title-subpage upper" style="color:#03346f; <?php if(is_page_template('page-templates/template-projekty-uchwal.php')) { echo "display: none";}?>">
          <?php the_archive_title(); ?>
        </h3>
             <hr>
              <?php  if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
              	<?php get_template_part( 'content' ); ?>
							<?php endwhile;
                          endif; ?>

              <div class="pagi">
                <?php
                  $pagination = get_paginate_links(array(
                    'format' => 'page/%#%',
                    'current' => max(1, get_query_var('paged')),
                    'total' => $wp_query->max_num_pages,
                  ));
                  echo get_template_part_args('partials/pagination', array('pagination' => $pagination, 'max_pages' => $wp_query->max_num_pages));
                ?>
              </div>
            </div>
            <div class="sidebar col-md-3 col-sm-4 col-xs-12">
            
			<?php 
				$taxonomy = get_the_taxonomies(get_the_id());
				foreach($taxonomy as $i => $val) {
					$taxonomy = $i;
				}
				display_custom_category($taxonomy);
			?>
            <?php
						if ( is_active_sidebar( 'sidebar_blog' ) ) {
							dynamic_sidebar( 'sidebar_blog' );
						}?>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>
