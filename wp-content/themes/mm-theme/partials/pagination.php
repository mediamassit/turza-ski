<?php if($max_pages > 1) : ?>
	

	<ul class="pagination">
		<?php foreach($pagination as $link) : ?>
		<?php if($link['current']) : ?>
		<li><a href="#" class="active"><?php echo $link['text']; ?></a></li>
		<?php elseif($link['prev']) : ?>
		<li><a href="<?php echo $link['link']; ?>" class="nav-prev"></a></li>
		<?php elseif($link['next']) : ?>
		<li><a href="<?php echo $link['link']; ?>" class="nav-next"></a></li>
		<?php else : ?>
		<li><a href="<?php echo $link['link']; ?>"><?php echo $link['text']; ?></a></li>
		<?php endif; ?>
		<?php endforeach; ?>
	</ul>

<?php endif; ?>
