<?php

	global $post, $wp_query;

	$breadcrumbs = array(
		array(get_bloginfo('url'), __('Strona główna', 'dmx-theme'))
	);

	if(is_array(@$pages)) {
		$breadcrumbs = array_merge($breadcrumbs, $pages);
	}
	else {
		if(is_single() && is_foreachable($categories = get_the_category())) {
			$category = $categories[0];
			$breadcrumbs[] = array(get_term_link($category->term_id, $category->taxonomy), $category->name);
			unset($category);
		}

		if(is_home()) {
			$page = get_page(get_option('page_for_posts'));
			$breadcrumbs[] = array(get_permalink($page->ID), $page->post_title);
			unset($post_type);
		}

		if(is_singular('dx_product') || is_tax('dx_product_cat') || is_post_type_archive('dx_product')) {
			$post_type = get_post_type_object('dx_product');
			$breadcrumbs[] = array(get_post_type_archive_link('dx_product'), @$post_type->labels->name);
			unset($post_type);

			if(is_tax('dx_product_cat')) {
				$category = $wp_query->queried_object;
				$breadcrumbs[] = array(get_term_link($category->term_id, $category->taxonomy), $category->name);
				unset($category);
			}

			if(is_singular('dx_product') && is_foreachable($categories = get_the_terms($post->ID, 'dx_product_cat'))) {
				$category = $categories[0];
				$breadcrumbs[] = array(get_term_link($category->term_id, $category->taxonomy), $category->name);
				unset($category);
			}
		}

		if(is_tax('dx_product_type') || is_tax('dx_product_producer')) {
			$breadcrumbs[] = array('#', get_queried_object()->name);
		}

		if(is_singular('dx_company') || is_tax('dx_company_type')) {
			if(is_tax('dx_company_type')) {
				$category = $wp_query->queried_object;
				$breadcrumbs[] = array(get_term_link($category->term_id, $category->taxonomy), $category->name);
				unset($category);
			}

			if(is_singular('dx_company') && is_foreachable($categories = get_the_terms($post->ID, 'dx_company_type'))) {
				$category = $categories[0];
				$breadcrumbs[] = array(get_term_link($category->term_id, $category->taxonomy), $category->name);
				unset($category);
			}
		}

		if(is_singular('dx_designer') || is_post_type_archive('dx_designer')) {
			$post_type = get_post_type_object('dx_designer');
			$breadcrumbs[] = array(get_post_type_archive_link('dx_designer'), @$post_type->labels->name);
			unset($post_type);
		}

		if(is_singular('dx_developer') || is_post_type_archive('dx_developer')) {
			$post_type = get_post_type_object('dx_developer');
			$breadcrumbs[] = array(get_post_type_archive_link('dx_developer'), @$post_type->labels->name);
			unset($post_type);
		}

		if(is_singular('dx_producer') || is_post_type_archive('dx_producer')) {
			$post_type = get_post_type_object('dx_producer');
			$breadcrumbs[] = array(get_post_type_archive_link('dx_designer'), __('Nasze sklepy', 'dmx-theme'));
			unset($post_type);
		}

		/*if(is_singular('dx_arrangement') || is_post_type_archive('dx_arrangement')) {
			$post_type = get_post_type_object('dx_arrangement');
			$breadcrumbs[] = array(get_post_type_archive_link('dx_arrangement'), @$post_type->labels->name);
			unset($post_type);
		}*/
		if(is_singular('dx_arrangement') || is_tax('dx_arrangement_cat') || is_post_type_archive('dx_arrangement')) {
			$post_type = get_post_type_object('dx_arrangement');
			$breadcrumbs[] = array(get_post_type_archive_link('dx_arrangement'), @$post_type->labels->name);
			unset($post_type);

			if(is_tax('dx_arrangement_cat')) {
				$category = $wp_query->queried_object;
				$breadcrumbs[] = array(get_term_link($category->term_id, $category->taxonomy), $category->name);
				unset($category);
			}

			if(is_singular('dx_arrangement') && is_foreachable($categories = get_the_terms($post->ID, 'dx_arrangement_cat'))) {
				$category = $categories[0];
				$breadcrumbs[] = array(get_term_link($category->term_id, $category->taxonomy), $category->name);
				unset($category);
			}
		}

		if(is_post_type_archive('post')) {
			$term = get_queried_object();
			$breadcrumbs[] = array(null, $term->taxonomy ? $term->name : get_the_archive_title());
		}

		if(is_search()) {
			$breadcrumbs[] = array(get_permalink(), get_the_title());
			$breadcrumbs[] = array(null, get_search_query());
		}

		if(is_single() && get_the_title()) {
			$breadcrumbs[] = array(get_the_permalink(), get_the_title());
		}

		if(is_page()) {
			$ancestors = array_reverse(get_ancestors($post->ID, $post->post_type));
			
			if(is_foreachable($ancestors)) {
				foreach($ancestors as $ancestor) {
					$breadcrumbs[] = array(get_permalink($ancestor), get_the_title($ancestor));
				}
			}

			$breadcrumbs[] = array(get_the_permalink(), get_the_title());
		}
	}

?>
				<div class="breadcrumbs">
					<ul>
						<?php foreach($breadcrumbs as $key => $page) : $is_last = $key === sizeof($breadcrumbs)-1; ?>
						<?php if(!$is_last) : ?>
						<li><?php if($page[0]) : ?><a href="<?php echo $page[0]; ?>"><?php endif; ?><?php echo $page[1]; ?><?php if($page[0]) : ?></a><?php endif; ?></li>
						<?php else : ?>
						<li><?php echo $page[1]; ?></li>
						<?php endif; ?>
						<?php endforeach; ?>
					</ul>
				</div>
				<!-- .breadcrumbs -->