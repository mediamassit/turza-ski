<div class="content-top-bar">
	<?php if(is_single()) : ?>
	<div class="pull-left">
		<p><strong><?php the_time('j M Y'); ?></strong></p>
	</div>
	<?php endif; ?>
	<div class="pull-right">
		<p class="print hide@xs"><a href="#" class="printomatic" id="my_print_button" data-print_target=".content"><?php _e('wydrukuj',THEME_NAME); ?> <i class="icon icon-printer"></i></a>
		<?php echo do_shortcode('[print-me print_only=".content" id="my_print_button" do_not_print=".content-top-bar, form" printstyle="external"]'); ?>
		</p>
		<p class="size-font">
			<span><?php _e('rozmiar czcionki',THEME_NAME); ?></span>
			<a href="#" class="size-minus"><i class="icon icon-minus"></i></a>
			<a href="#" class="size-plus"><i class="icon icon-plus"></i></a>
		</p>
	</div>
</div>