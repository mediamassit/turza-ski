<?php
global $wp_query;

$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$posts_per_page = get_query_var('posts_per_page');
if(is_front_page()) $posts_per_page = 4;
$args = array(
	'post_type' => 'post',
	'category_name' => 'wszystkie-aktualnosci',
	'posts_per_page' => $posts_per_page,
	'paged' => $paged
);

$news = new WP_Query($args);
?>
<?php if ( $news->have_posts() ) : ?>
<div class="news">
	<div class="title">
		<h2><?php _e('Aktualności',THEME_NAME); ?></h2>
		<?php get_template_part_args('partials/top-bar-news'); ?>
	</div>
	<?php while ( $news->have_posts() ) : 
	$news->the_post(); ?>
		<?php get_template_part( 'content', 'news' ); ?>
	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
	<?php
		if(!is_front_page()) {
			$pagination = get_paginate_links(array(
				'format' => 'page/%#%',
				'current' => max(1, get_query_var('paged')),
				'total' => $news->max_num_pages,
			));

			echo get_template_part_args('partials/pagination', array('pagination' => $pagination, 'max_pages' => $news->max_num_pages));
		}
	?>
</div>
<?php else: ?>
	<h1><?php the_title(); ?></h1>
	<p><?php _e('Brak wpisów na blogu.',THEME_NAME); ?></p>
<?php endif; ?>