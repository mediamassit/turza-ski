<?php
/**
 * Theme init.
 *
 * @since 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

// load constants.
require_once trailingslashit( get_template_directory() ) . 'src/constants.php';

// Setup theme.
require_once SRC_DIR . '/theme-setup.php';

// Helpers functions
require_once SRC_DIR . '/helpers.php';

// Navigation Walker
require_once SRC_DIR . '/navigation-walker.php';

// Extension
require_once EXT_DIR . '/breadcrumb.php';
require_once EXT_DIR . '/other.php';
require_once EXT_DIR . '/gallery-wp.php';
require_once EXT_DIR . '/additional.php';

// Widgets
require_once SRC_DIR . 'widgets/menu.php';
require_once SRC_DIR . 'widgets/form.php';

// Shortcodes
require_once SRC_DIR . 'shortcodes/accordion.php';