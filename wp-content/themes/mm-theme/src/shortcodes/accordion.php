<?php
function accordion_open_func( $atts ) {

	return '<div class="accordion">';
}
add_shortcode( 'accordion_open', 'accordion_open_func' );

function accordion_close_func( $atts ) {
	return '</div>';
}
add_shortcode( 'accordion_close', 'accordion_close_func' );

function accordion_item_func( $atts, $content = null ) {
	$a = shortcode_atts( array(
		'title' => '',
	), $atts );
	$output = '<h3>'.$a['title'].'</h3>';
	$output .= '<div>'. $content.'</div>';

	return $output;
}
add_shortcode( 'accordion_item', 'accordion_item_func' );