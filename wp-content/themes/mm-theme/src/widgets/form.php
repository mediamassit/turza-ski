<?php

class MM_Widget_Form extends WP_Widget {

	public function __construct() {
		$widget_ops = array('classname' => 'widget-form theme-dark', 'description' => __('Formularz CF7', THEME_NAME));
		$control_ops = array('width' => 400, 'height' => 350);
		parent::__construct('dmx-promotion', __('Formularz CF7', THEME_NAME), $widget_ops, $control_ops);
	}

	public function widget( $args, $instance ) {
	if(is_front_page()) {
		echo '<div class="widget widget-form hide show@xs">';
	} else {
		echo '<div class="widget widget-form">';
	}

	$form = get_field('contact_form_page','option');
	display_cf7($form); 

	echo '</div>';
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['text'] = $new_instance['text'];
		return $instance;
	}

	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'text' => '' ) );
?>
		<p>Formularz ustawiony w opcjach witryny.</p>
<?php
	}
}

function mm_widget_form_init() {
	register_widget('MM_Widget_Form');
}
add_action('widgets_init', 'mm_widget_form_init');