<?php
/**
 * Theme setup.
 *
 * @package mm
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function mm_theme_setup() {

	load_theme_textdomain(THEME_NAME, THEME_DIR . '/languages');

	/**
	 * Load editor style.
	 */
	add_editor_style();

	/**
	 * Enable support for Post Thumbnails.
	 */
	add_theme_support('post-thumbnails');

	/**
	 * Enable support for Post Formats.
	 */
	//add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status' ) );

	/**
	 * Enable support for menus.
	 */
	add_theme_support('widgets');

	/**
	 * Enable support for widgets.
	 */
	add_theme_support('widgets');

	/**
	 * Register custom menu.
	 */
	register_nav_menus( array(
		'primary'     => _x( 'Primary Menu', 'backend', THEME_NAME ),
		'footer'      => _x( 'Footer Menu', 'backend', THEME_NAME ),
		'about_us'      => _x( 'Sidebar About', 'backend', THEME_NAME ),
		'elections_sidebar' => _x('Sidebar Wybory', 'backend', THEME_NAME ),
		'wybory_2010' => _x('Sidebar wybory 2010', 'backend', THEME_NAME ),
	) );

	/**
	 * Additional image size.
	 */
	add_image_size('thumb-240x300', 240, 300, true);
	add_image_size('thumb-420x320', 420, 320, true);
	//add_image_size('thumb-600x420', 600, 420, true);
	//add_image_size('thumb-800x400', 800, 400, false);
	add_image_size('thumb-220x190', 220, 190, false);
	add_image_size('thumb-870x415', 870, 415, true);
	add_image_size('thumb-175x115', 175, 115, true);
	/**
	 * Enable crop.
	 */
	if(!get_option('medium_crop')) {
		update_option('medium_crop', '1');
	}
}
add_action('after_setup_theme', 'mm_theme_setup', 10);

/**
 * Register widgetized areas.
 */
function mm_widgets_init() {

	$params = array(
		'before_widget' => '<div class="widget">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="subtitle">',
		'after_title' => '</h3>',
	);

	$areas = array(
		'about' => array(
			'name' => __( 'Sidebar: O nas', THEME_NAME ),
			'desc' => __( 'Widgety widoczne w panelu bocznym na stronie aktualności.', THEME_NAME ),
		),
		'blog' => array(
			'name' => __( 'Sidebar: Blog', THEME_NAME ),
			'desc' => __( 'Widgety widoczne w panelu bocznym na stronie aktualności.', THEME_NAME ),
		),
    'umwetu' => array(
			'name' => __( 'Sidebar: Projekt UMWETU', THEME_NAME ),
			'desc' => __( 'Widgety widoczne w panelu bocznym na stronie aktualności.', THEME_NAME ),
		),
	'wyszukiwarka' => array(
			'name' => __( 'Sidebar: Wyszukiwarka', THEME_NAME ),
			'desc' => __( 'Widgety widoczne w panelu bocznym', THEME_NAME ),
		),
	'news' => array(
			'name' => __( 'Sidebar: Aktualności', THEME_NAME ),
			'desc' => __( 'Widgety widoczne w panelu bocznym: Aktualności', THEME_NAME ),
		),
	'elections' => array(
			'name' => __( 'Sidebar: Wybory', THEME_NAME ),
			'desc' => __( 'Widgety widoczne w panelu bocznym: Sidebar wybory', THEME_NAME ),
		),
	'default' => array(
			'name' => __( 'Sidebar: Domyślny', THEME_NAME ),
			'desc' => __( 'Widgety widoczne w panelu bocznym: Sidebar domyślny', THEME_NAME ),
		),
	);

	if ( ! empty( $areas ) && is_array( $areas ) ) {
		$prefix = 'sidebar_';

		foreach( $areas as $sidebar_id=>$sidebar ) {
			$sidebar_args = array(
				'name'          => ( isset( $sidebar['name'] ) ? $sidebar['name'] : '' ),
				'id'            => $prefix . $sidebar_id,
				'description'   => ( isset( $sidebar['desc'] ) ? $sidebar['desc'] : '' ),
				'before_widget' => $params['before_widget'],
				'after_widget'  => $params['after_widget'],
				'before_title'  => $params['before_title'],
				'after_title'   => $params['after_title'],
			);
			register_sidebar( $sidebar_args );
		}
	}
}
add_action( 'widgets_init', 'mm_widgets_init' );

/**
 * Register scripts and styles.
 */
function mm_enqueue_scripts() {

	wp_enqueue_style('mm-assets-style', THEME_URI.'/assets/css/style.css', array(), '1.0.0');

	wp_enqueue_style('mm-style', get_stylesheet_uri(), array(), '1.0.0');


	wp_enqueue_script('mm-vendor', THEME_URI.'/assets/js/vendor.js', array(), '1.0.0', true);

	wp_enqueue_script('mm-app', THEME_URI.'/assets/js/app.js', array(), '1.0.0', true);

}
add_action('wp_enqueue_scripts', 'mm_enqueue_scripts');

/**
 * Excerpt settings
 */
function mm_excerpt_more($more) {
	return '...';
}
add_filter('excerpt_more', 'mm_excerpt_more');

function mm_excerpt_length($length) {
	return 30;
}
add_filter('excerpt_length', 'mm_excerpt_length', 999);

/**
 * Remove default gallery styling
 */
add_filter('use_default_gallery_style', '__return_false');

/**
 * Add TinyMCE style
 */
function mm_mce_config($init) {
	$style_formats = array (
		array('title' => 'Lead', 'inline' => 'span', 'classes' => 'lead')
	);
	$init['style_formats'] = json_encode($style_formats);
	$init['style_formats_merge'] = false;

	return $init;
}
add_filter('tiny_mce_before_init', 'mm_mce_config');

/**
 * Custom preloader for CF7
 */
// function mm_wpcf7_ajax_loader() {
// 	return get_template_directory_uri().'/assets/images/preloader.gif';
// }
// add_filter('wpcf7_ajax_loader', 'mm_wpcf7_ajax_loader');

/**
 * Login panel WP styles.
 */
function mm_login_styles() { ?>
<style type="text/css">
	.login h1 a {
		background-image: url('<?php echo THEME_URI; ?>/assets/images/logo.png');
		background-size: auto;
		display: block;
		width: 129px;
		height: 109px;
	}
</style>
<?php }
add_action('login_enqueue_scripts', 'mm_login_styles');

/**
 * Login panel WP change title.
 */
function mm_change_title_on_logo() {
	return get_bloginfo('title');
}
add_filter('login_headertitle', 'mm_change_title_on_logo');

/**
 * Login panel WP change url.
 */
function mm_loginpage_custom_link() {
	return get_bloginfo('url');
}
add_filter('login_headerurl', 'mm_loginpage_custom_link');
