<?php

function display_article($image, $desc, $link) {

	$output = '<div class="promo-article">';
	if($image) {
		$output .= '<div class="row"><div class="gr-3 no-gutter-right"><figure>';
		$output .= '<img src="'.$image['sizes']['thumb-240x300'].'" alt="" />';
		$output .= '</figure></div>';
	}
	if($image) {
		$output .= '<div class="gr-7"><div class="description">';
	} else {
		$output .= '<div class="description without-image">';
	}
	$output .= $desc;
	$output .= '<a href="'.esc_attr($link).'" class="see-more more more-blue">'.__('czytaj dalej',THEME_NAME).'</a>';
	if($image) {
		$output .= '</div></div></div>';
	} else {
		$output .= '</div>';
	}
	$output .= '</div>';

	print $output;

}

function display_banner($type) {
	$place1 = get_field('place1','option');
	$place2 = get_field('place2','option');
	$place1_phone = get_field('place1_phone','option');
	$place2_phone = get_field('place2_phone','option');

	if($type == 'home') {
		$place1 = get_field('place1_home','option');
		$place2 = get_field('place2_home','option');
		$format = __('Rejestracja osobista<span class="number"> lub telefoniczna</span> pod numerem telefonu <strong>w&nbsp;%s <span class="number numberbb">%s</span></strong> w <strong>%s <span class="number numberjaw">%s</span></strong>',THEME_NAME);
		$output = '<p>';
		$output .= sprintf($format, $place1, $place1_phone, $place2, $place2_phone);
		$output .= '</p>';
	} else if($type == 'mobile') {
		$format = __('<strong>%s <span class="number numberbb">%s</span></strong><br/><br/><strong>%s <span class="number numberjaw">%s</span></strong>',THEME_NAME);
		$output = '<p>';
		$output .= sprintf($format, $place1, $place1_phone, $place2, $place2_phone);
		$output .= '</p>';
	} else {
		$output = '<div class="banner hide@xs"><div class="row"><div class="gr-3 prefix-3">';
		$output .='<h3>'.__('rejestracja telefoniczna',THEME_NAME).'</h3>';
		$output .= '</div><div class="gr-4">';
		$output .= '<h5>'.$place1.'</h5>';
		$output .= '<p class="number numberbb">'.$place1_phone.'</p>';
		$output .= '<h5>'.$place2.'</h5>';
		$output .= '<p class="number numberjaw">'.$place2_phone.'</p>';
		$output .= '</div></div></div>';
	}

	print $output;
}

function display_owl_slider($name, $type, $class) {
	$prefix = "slider_";

	if(!$name || !$type) {
		return false;
	}
	$slider_name = $prefix.$name;

	$slider = get_field($slider_name);

	if($name == 'partners') {
		$slider = get_field($slider_name,'option');
	}

	if(isset($slider) && is_array($slider)) {

		$slider_html = '<div id="'.$slider_name.'" class="owl-carousel '.$class.'">';

		switch($type) {
			case 'image' :
				foreach ($slider as $i => $slide) {
					if($slide['desc']) $desc =  $slide['desc'];
					if($slide['image']) $image =  $slide['image'];
					if($slide['link']) $link =  $slide['link'];

					$slider_html .= '<div class="item"><figure>';
					$slider_html .= '<img src="'.$image['sizes']['large'].'" alt="'.$image['alt'].'" />';
					if($desc) {
						$slider_html .= '<figcaption><p>';
						$slider_html .= $desc;
						if($link) {
							$slider_html .= '<a href="'.esc_attr($link).'">'.__('czytaj dalej', THEME_NAME).'</a>';
						}
						$slider_html .= '</p></figcaption>';
					}
					$slider_html .= '</figure>';
					$slider_html .= '</div>';
				}
			break;
			case 'text' :
				foreach ($slider as $i => $slide) {
					if($slide['desc']) $desc =  $slide['desc'];
					if($slide['author']) $author =  $slide['author'];

					$slider_html .= '<div class="item">';
					if(isset($author)) {
						$slider_html .= '<h3>';
						$slider_html .= $author;
						$slider_html .= '</h3>';
					}
					$slider_html .= $desc;
					$slider_html .= '</div>';
				}
			break;
			case 'partners' :
				foreach ($slider as $i => $slide) {
					if($slide['link']) $link =  $slide['link'];
					if($slide['image']) $image =  $slide['image'];

					$slider_html .= '<div class="item">';
					$slider_html .= '<a href="'.$link.'" target="_blank">';
					$slider_html .= '<img src="'.$image['sizes']['large'].'" alt="'.$image['alt'].'" />';
					$slider_html .= '</a>';
					$slider_html .= '</div>';
				}
			break;
			default :
			return false;
		}

		$slider_html  .= '</div>';
		print $slider_html;
	}
}



function display_categories($id, $single = false) {
	if($single === true) {
		$wpseo_primary_term = new WPSEO_Primary_Term( 'category', $id );
		$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
		$primary_cat = get_term( $wpseo_primary_term );
		if(!$primary_cat->errors > 0) {
			$args = array('parent' => $primary_cat->term_id,'orderby' => 'menu_order');
			$categories = get_categories( $args );
		} else {
			$categories = '';
		}
	} else {
		$cat_current = get_query_var('cat');

		$category = get_category($cat_current);
		if($category->parent && $category->parent != 0) {
			$args = array('parent' => $category->parent, 'orderby' => 'menu_order');
		} else {
			$args = array('parent' => $cat_current,'orderby' => 'menu_order');
		}
		$categories = get_categories( $args );
	}
	if(is_foreachable($categories) && !(sizeof($categories) == 1 && $categories[0]->term_id == $cat_current)) {
		$output .= '<div class="">';
		$output .= '<h3 class="subtitle">'.__('Kategorie',THEME_NAME).'</h3>';
		$output .= '<ul class="category">';

		foreach($categories as $cat) {
			$cat_url = get_term_link($cat);
			if($cat_current != $cat->term_id) {
				$output .= '<li><a href="'.$cat_url.'">';
				$output .= $cat->name;
				$output .= ' <span class="count">('.$cat->count.')</span>';
				$output .= '</a></li>';
			}
		}
		$output .= '</ul>';
		$output .= '</div>';
	}
	print $output;
}

function display_main_categories() {
	$args = array('parent' => 0,'orderby' => 'menu_order');
	$categories = get_categories( $args );
	if(is_foreachable($categories)) {
		$output .= '<div class="">';
		$output .= '<h3 class="subtitle">'.__('Kategorie',THEME_NAME).'</h3>';
		$output .= '<ul class="category">';

		foreach($categories as $cat) {
			$cat_url = get_term_link($cat);
			if($cat_current != $cat->term_id) {
				$output .= '<li><a href="'.$cat_url.'">';
				$output .= $cat->name;
				$output .= ' <span class="count">('.$cat->count.')</span>';
				$output .= '</a></li>';
			}
		}
		$output .= '</ul>';
		$output .= '</div>';
	}
	print $output;
}

function display_custom_category($taxonomy) {
	  $terms = get_terms( array(
					'taxonomy' => $taxonomy,
					'hide_empty' => false,
				) ); 
if(is_foreachable($terms)) {
		$output .= '<div class="">';
		$output .= '<h3 class="subtitle">'.__('Kategorie',THEME_NAME).'</h3>';
		$output .= '<ul class="category">';

		foreach($terms as $cat) {
			$cat_url = get_term_link($cat);
			if($cat_current != $cat->term_id) {
				$output .= '<li><a href="'.$cat_url.'">';
				$output .= wp_trim_words( $cat->name, 3, '...');
				$output .= ' <span class="count">('.$cat->count.')</span>';
				$output .= '</a></li>';
			}
		}
		$output .= '</ul>';
		$output .= '</div>';
	}
	print $output;

}


function display_tabs_inter($taxonomy, $id) {
	  $terms = get_terms( array(
					'taxonomy' => $taxonomy,
					'hide_empty' => false,
				) ); 
if(is_foreachable($terms)) {
	
		foreach($terms as $cat) {
			if($id == $cat->term_id): $class = 'active-dt'; else: $class = " "; endif;
			$cat_url = get_term_link($cat);
			if($cat_current != $cat->term_id) {
				$output .= '<a style="margin-left:10px;" class="date-change '.$class.'"  href="'.$cat_url.'">';
				$output .= wp_trim_words( $cat->name, 3, '...');
				$output .= '</a>';
			}
		}
	}
	print $output;

}
