<?php
function display_breadcrumb() {
	global $post, $wp_query;
	$breadcrumbs = array(
		array(get_bloginfo('url'), __('Strona główna', THEME_NAME))
	);
	if(is_array(@$pages)) {
		$breadcrumbs = array_merge($breadcrumbs, $pages);
	}
	else {
		if(is_single() && is_foreachable($categories = get_the_category())) {
			$category = $categories[0];
			if($category->term_id == 9) {
				$link = get_field('media_link', 'option');
				$breadcrumbs[] = array($link, $category->name);
			} else {
				$link = get_field('blog_link', 'option');
			 	$breadcrumbs[] = array($link, $category->name);
			 }
			unset($category);
		}

		if(is_front_page()) {
			array_shift($breadcrumbs);
		}

		if(is_post_type_archive('post')) {
			$term = get_queried_object();
			$breadcrumbs[] = array(null, $term->taxonomy ? $term->name : get_the_archive_title());
		}

		if(is_search()) {
			$breadcrumbs[] = array(get_permalink(), get_the_title());
			$breadcrumbs[] = array(null, get_search_query());
		}

		if(is_single() && get_the_title()) {
			$breadcrumbs[] = array(get_the_permalink(), get_the_title());
		}

		if(is_page()) {
			$ancestors = array_reverse(get_ancestors($post->ID, $post->post_type));
			
			if(is_foreachable($ancestors)) {
				foreach($ancestors as $ancestor) {
					$breadcrumbs[] = array(get_permalink($ancestor), get_the_title($ancestor));
				}
			}

			$breadcrumbs[] = array(get_the_permalink(), get_the_title());
		}
	}
	$output = '<div class="breadcrumb">';
	$output .= '<ul>';
	foreach($breadcrumbs as $key => $page) { 
		$is_last = $key === sizeof($breadcrumbs)-1; 
		if(!$is_last) {
			$output .= '<li>';
			$output .= ($page[0]) ? '<a href="'.$page[0].'">' : '';
			$output .= $page[1];
			$output .= ($page[0]) ? '</a>' : '';
			$output .= '</li>';
		} else {
			$output .= '<li>'.$page[1].'</li>';
		}
	}
	$output .= '</ul>';
	$output .= '</div>';

	echo $output;
}