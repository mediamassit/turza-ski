<?php
add_filter('post_gallery','customFormatGallery',10,2);

function customFormatGallery($string,$attr){

	$output = "<div id=\"container\">";
	$posts = get_posts(array('include' => $attr['ids'],'post_type' => 'attachment', 'orderby' => 'post__in'));

	if(is_foreachable($posts)) {
		if(!is_page_template('page-templates/template-testimonials.php')) {
			$output =  '<div class="gallery"><div class="row">';
			foreach($posts as $imagePost){
				$image = wp_get_attachment_image_src($imagePost->ID, 'full');
				$image2 = wp_get_attachment_image_src($imagePost->ID, 'thumb-190x140');
				$output .= '<div class="item gr-5@xxs gr-10@xs">';
				$output .= '<a href="'.$image[0].'" title="'.$imagePost->post_excerpt.'">';
				$output .= '<img src="'.$image2[0].'" alt="" />';
				$output .= '</a>';
				$output .= '<span>'.$imagePost->post_excerpt.'</span>';
				$output .= '</div>';

			}
			$output .= "</div></div>";
		} else {
			$output =  '<div class="testiomonials">';
			foreach($posts as $imagePost){
				$image = wp_get_attachment_image_src($imagePost->ID, 'full');
				$image2 = wp_get_attachment_image_src($imagePost->ID, 'large');
				$output .= '<div class="item">';
				if($imagePost->post_excerpt) {
					$output .= '<h4>'.$imagePost->post_excerpt.'</h4>';
				}
				$output .= '<a href="'.$image[0].'" title="'.$imagePost->post_excerpt.'">';
				$output .= '<img src="'.$image2[0].'" alt="" />';
				$output .= '</a>';
				$output .= '</div>';
			}
			$output .= "</div>";
		}
	}

	return $output;
}