<?php get_header(); ?>
<section class="bg">
	<div class="slider-bg-blog"></div>
	<div class="slider-bg-1-blog"></div>
	<div class="title-slider">
		<h2><?php _e('404', THEME_NAME); ?> <br> <span class="letter"><?php _e('Wskazana podstrona nie istnieje.', THEME_NAME); ?></span><br><a href="<?php echo home_url() ;?>"><span style="line-height:130px;font-size:34px;"><?php _e('Powrót do strony głównej', THEME_NAME); ?></span></a></h2>
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis_07.png" alt="Krzysztof Turzański" class="krzysztof">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis-slawa_03.png" alt="Sława Umińska-Duraj" class="slawa">
	</dv>
</section>

<?php get_footer(); ?>