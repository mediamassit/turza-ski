;(function($) {
	$(document).ready(function() {
		 $('.popup-gallery a, .main-post-full a.image-link').magnificPopup({
			 type: 'image',
			 closeOnContentClick: false,
			 closeBtnInside: false,
			 mainClass: 'mfp-with-zoom mfp-img-mobile',
			 image: {
				 verticalFit: true
			 },
			 gallery: {
				 enabled: true
			 },
			 zoom: {
				 enabled: true,
				 duration: 300, // don't foget to change the duration also in CSS
				 opener: function(element) {
					 return element.find('img');
				 }
			 }
		 });
		 $('.main-post-full > a').magnificPopup({
				type: 'image',
				closeOnContentClick: true,
				image: {
					verticalFit: false
				}
			});
		$.mCustomScrollbar.defaults.scrollButtons.enable=true;
		$(".content").mCustomScrollbar({
			theme:"rounded-dots-dark",
			scrollButtons: {enable: true}
		});
	});

		$(".hamburger").on('click', function() {
				$("#mobile").fadeToggle().toggleClass('hidden');
		});

  $('.wrapper-buttons-change a').click(function (e) {
  	e.preventDefault()
  	$(this).tab('show')
  });
})(jQuery);
