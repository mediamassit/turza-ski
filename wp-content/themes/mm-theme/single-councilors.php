<?php

get_header();

?>


<section class="bg">
    <div class="slider-bg-blog">
    </div>
    <div class="slider-bg-1-blog">
    </div>

    <div class="title-slider">
        <h2>KRZYSZTOF TURZAŃSKI <br> <span class="letter">SŁAWA UMIŃSKA-DURAJ</span></h2>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis_07.png" alt="Krzysztof Turzański" class="krzysztof">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis-slawa_03.png" alt="Sława Umińska-Duraj" class="slawa">
    </div>
</section>
<?php
	$photo = get_field('photo_councilor');
    $name = get_field('name_councilor');
    $desc = get_field('desc');
    $phone = get_field('phone_councilor');
    $mail = get_field('mail_councilor');
    $meet_data = get_field('meet_data');
    $meet_localization = get_field('meet_localization');
    $meet_time = get_field('meet_time');
    $conclusions = get_field('conclusions');
;?>
<main id="page-main">
    <div class="row">
        <div class="container">
            <div class="okr-single-content col-md-9 col-sm-8 col-xs-12">
                <div class="av"><img src="<?php echo $photo['sizes']['thumb-220x190'] ;?>" alt=""></div>
                <div class="desc-okr-single">
                    <h3><?php echo $name ;?></h3>
                    <?php
                    $count = 0;
                    $cat = get_field('check_person');


                $args = array(
                  'post_type' => 'interpelations',
					'posts_per_page' => 999,
                   'tax_query' => array(
                    array(
                         array(
                        'taxonomy' => 'radni_category',
                        'field'    => 'slug',
                        'terms'    => $cat
                       ),
                    ),
                  ),
                );
                $query = new WP_Query( $args );
              ?>
                        <?php /*
              $count=0;
              if($conclusions):
              foreach($conclusions as $proposal):
                $count++;
              endforeach;
              endif;
              */ ;?>
            <h4><?php _e('Liczba wniosków i interpelacji: ', THEME_NAME) ;?><?php echo $query ->found_posts;?></h4>
            <?php echo $desc ;?>
            <h4><?php _e('Kontakt:', THEME_NAME) ;?></h4>
						<p class="contact"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/phone_03.png" alt=""><a href="tel:<?php echo $phone ;?>"><?php echo $phone; ?></a></p>
						<p class="contact"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/email_06.jpg" alt=""><a href="mailto:<?php echo $mail ;?>"><?php echo $mail; ?></a></p>
            <?php if($meet_data && $meet_localization && $meet_time): ?>
              <h4 class="meet-okr"><?php _e('Spotkania z mieszkańcami', THEME_NAME) ;?></h4>
              <p class="contact"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/calendar_03.png" alt=""><?php echo $meet_data ;?></p>
              <p class="contact"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/place_06.png" alt=""><?php echo $meet_localization ;?></p>
              <p class="contact"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/time_08.png" alt=""><?php echo $meet_time ;?></p>
            <?php endif; ?>
            <h4 class="meet-okr"><?php _e('Złożone wnioski i interpelacje:', THEME_NAME);?></h4>

                    <!--pobranie z interpolacji -> wnioski i interpolacje-->

            <?php if ( have_posts($query) ) : while ( $query->have_posts() ) : $query->the_post();

            ?>
              <a href="<?php the_permalink();?>"><p class="contact"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/download_10.png"><?php echo the_title() ;?></p></a>
            <?php
            endwhile;
            else: echo 'brak wniosków';
            endif
            ;?>
                </div>

            </div>
            <div class="sidebar col-md-3 col-sm-4 col-xs-12">
                <?php
                if ( is_active_sidebar( 'sidebar_about' ) ) {
                  dynamic_sidebar( 'sidebar_about' );
                }
                ?>
        				<div class="social">
        					<ul>
        					<?php $socials = get_field('social_group',option);
        						if($socials):
        							foreach($socials as $social):
        						;?>
        						<li class="item" style="background-image: url(<?php echo $social['social_icon']; ?>)"><a href="<?php echo $social['social_link']; ?>"></a></li>
        					<?php endforeach; endif ;?>
        					</ul>
        				</div>
            </div>
        </div>
    </div>
</main>



<?php get_footer(); ?>
