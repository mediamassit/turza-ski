<?php

get_header();
$pinned = get_field('pinned_post',option) ;
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
	'post_type' => 'post',
	'posts_per_page' => 8,
	'paged' => $paged,
	'post__not_in' => array( $pinned->ID, )
);
$loop = new WP_Query( $args );

$short_content = get_field('short_content');
?>
 <section class="bg">
    <div class="slider-bg-blog">
    </div>
    <div class="slider-bg-1-blog">
    </div>

    <div class="title-slider">
        <h2>KRZYSZTOF TURZAŃSKI <br> <span class="letter">SŁAWA UMIŃSKA-DURAJ</span></h2>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis_07.png" alt="Krzysztof Turzański" class="krzysztof">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis-slawa_03.png" alt="Sława Umińska-Duraj" class="slawa">
    </div>
</section>
<main>
    <div class="container">
        <div class="row">
            <div class="list-post col-md-9 col-sm-8 col-xs-12">
              <?php
              $args = array(
                'p' => $pinned->ID,
                'post_type' => 'post',
                'paged' => $paged
              );
              $pinn = new WP_Query( $args );?>
              <?php  if($paged == 1): while ( $pinn->have_posts() ) : $pinn->the_post();?>
              <?php $size_thumb = 'thumb-870x415'; ?>
                <div class="main-post col-md-12 col-sm-12 col-xs-12">
                    <div class="wrapper-image">
                      <a href="<?php the_permalink();?>"><img src="<?php echo get_the_post_thumbnail_url(get_the_id(),$size_thumb); ?>"></a>
                    </div>
                    <div class="description-post">
                        <p class="date"><?php echo get_the_time('d-m-Y', $post->ID); ?></p>
                        <a href="<?php the_permalink();?>"><h2><?php the_title(); ?></h2></a>
                        <p><?php echo wp_trim_words( get_the_content(), 30, '...' ); ?></p>
                        <p><a href="<?php the_permalink();?>">Czytaj więcej</a></p>
                    </div>
                    <p class="comments"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/comment_07.png" alt="">3 komentarze</p>
                </div>
                  <?php  endwhile; endif ?>
								 <?php  while ( $loop->have_posts() ) : $loop->the_post();?>
                          <?php get_template_part( 'content' ); ?>
                <?php  endwhile ;?>

                <div class="pagi">
                  <?php
                    $pagination = get_paginate_links(array(
                      'format' => 'page/%#%',
                      'current' => max(1, get_query_var('paged')),
                      'total' => $loop->max_num_pages,
                    ));
                    echo get_template_part_args('partials/pagination', array('pagination' => $pagination, 'max_pages' => $loop->max_num_pages));
                  ?>
                </div>
            </div>
          <div class="sidebar col-md-3 col-sm-4 col-xs-12">
			<?php
				if ( is_active_sidebar( 'sidebar_wyszukiwarka' ) ) {
					dynamic_sidebar( 'sidebar_wyszukiwarka' );
				}
			?>
            <?php display_categories(get_the_id()); ?>
            <?php
				if ( is_active_sidebar( 'sidebar_blog' ) ) {
					dynamic_sidebar( 'sidebar_blog' );
				}
			?>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>
