<?php get_header(); ?>
 <section class="bg">
    <div class="slider-bg-blog">
    </div>
    <div class="slider-bg-1-blog"></div>

    <div class="title-slider">
        <h2>KRZYSZTOF TURZAŃSKI <br> <span class="letter">SŁAWA UMIŃSKA-DURAJ</span></h2>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis_07.png" alt="Krzysztof Turzański" class="krzysztof">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/podpis-slawa_03.png" alt="Sława Umińska-Duraj" class="slawa">
    </div>
</section>
		<main>
			<div class="container">
				<div class="row">
					<div class="list-post col-md-9 col-sm-8 col-xs-12">
						<?php //display_breadcrumb(); ?>

<!--								<div class="news">-->

					<div class="title">
						<h2><?php _e('Wyszukiwarka',THEME_NAME); ?></h2>
					</div>
					<?php if (have_posts()) : ?>
						<h3 style="color:#03346f;"><?php printf( __( 'Wyniki wyszukiwania dla: %s', THEME_NAME), get_search_query() ); ?></h3>
						<h4 ><?php _e('Znaleziono:',THEME_NAME); ?> <?php echo $wp_query->found_posts; ?></h4>
						<hr>
						<?php while (have_posts()) : the_post() ; ?>
							<?php get_template_part( 'content' ); ?>
						<?php endwhile; ?>
						<?php
							$pagination = get_paginate_links(array(
								'format' => 'page/%#%',
								'current' => max(1, get_query_var('paged')),
								'total' => $wp_query->max_num_pages,
								));
								;?>
							<div class="pagi">
								<?php
								echo get_template_part_args('partials/pagination', array('pagination' => $pagination, 'max_pages' => $wp_query->max_num_pages));?>
							</div>
					<?php else: ?>
						<h3 style="color:#03346f;"><?php _e('Niczego nie znaleziono.',THEME_NAME); ?></h3>
							<hr>
						<?php /*<form class="search" action="<?php echo home_lang_url(); ?>/" style="display: block;">
								<input type="text" name="s" placeholder="SZUKAJ">
								<input type="submit" value="Szukaj">
						</form>*/;?>
					<?php endif; ?>
				</div>
<!--					</div>-->
					<!-- .gr -->
					  <div class="sidebar col-md-3 col-sm-4 col-xs-12">
                               <?php
                if ( is_active_sidebar( 'sidebar_wyszukiwarka' ) ) {
                  dynamic_sidebar( 'sidebar_wyszukiwarka' );
                } 
                ?>
                <?php
                if ( is_active_sidebar( 'sidebar_about' ) ) {
                  dynamic_sidebar( 'sidebar_about' );
                }
                ?>
						</div>
					<!-- .gr -->
				</div>
				<!-- .row -->
			</div>
			<!-- .container -->
		</main>

<?php get_footer(); ?>
