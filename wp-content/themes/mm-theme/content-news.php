<article>
	<header>
		<?php if(!is_search()) : ?>
			<span class="date"><?php the_time('j M Y'); ?></span>
		<?php endif; ?>
		<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
	</header>
	<div class="excerpt">
		<?php if(has_excerpt()): ?>
				<?php the_excerpt(); ?><a href="<?php the_permalink(); ?>" class="more more-blue"><?php _e('czytaj&nbsp;dalej',THEME_NAME); ?></a>
		<?php else: ?>
			<p><?php echo wp_trim_words( get_the_content(), 30, '...' ); ?> <a href="<?php the_permalink(); ?>" class="more more-blue"><?php _e('czytaj&nbsp;dalej',THEME_NAME); ?></a></p>
		<?php endif; ?>
	</div>
</article>