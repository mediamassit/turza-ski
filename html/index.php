<?php

/**
 * Return parsed contents of a given template file.
 *
 * @param string $path The path to template file
 * @return string
 */
function parse_template($path) {
	// If template file does not exists
	if (!file_exists($path)) {
		return false;
	}

	$contents = @file_get_contents($path);
	$dirname = strlen(dirname($path)) > 0 ? dirname($path) : '.';

	// If template file could not be loaded
	if (!$contents) {
		return false;
	}

	// Include defined template partials
	while(preg_match('#' . preg_quote('<!-- @include{') . '([^\{\}]+)' . preg_quote('} -->') . '#i', $contents, $match, PREG_OFFSET_CAPTURE) > 0) {
		$inclusion = parse_template($dirname . '/' . $match[1][0]);
		$contents = substr_replace($contents, $inclusion, $match[0][1], strlen($match[0][0]));
	}

	return $contents;
}

// Get a path to the template file
$file = isset($_GET['file']) ?
	'./' . $_GET['file'] : 
	'./index.html';

$content = parse_template($file);

if(!$content)
{
	header('HTTP/1.0 404 Not Found');
	die('<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>404 Not Found</title>
</head><body>
<h1>Not Found</h1>
<p>The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found on this server.</p>
</body></html>');
}

echo $content;